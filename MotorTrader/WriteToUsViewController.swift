//
//  WriteToUsViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class WriteToUsViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var messageTxtField: UITextField!
    @IBOutlet var phoneTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var nameTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTxtField.delegate = self
        emailTxtField.delegate = self
        phoneTxtField.delegate = self
        messageTxtField.delegate = self

            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
