//
//  CustomCellCarListTableViewCell.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CustomCellCarListTableViewCell: UITableViewCell {

  
   
    @IBOutlet var cellBackView: UIView!
    @IBOutlet var carModelYearImage: UIImageView!
    @IBOutlet var carLocationImage: UIImageView!
    @IBOutlet var carTypeImage: UIImageView!
    @IBOutlet var carModelNoImage: UIImageView!
    @IBOutlet var carModelYear: UILabel!
    @IBOutlet var carLocation: UILabel!
    @IBOutlet var carType: UILabel!
    @IBOutlet var carModelNo: UILabel!
    @IBOutlet var carPrice: UILabel!
    @IBOutlet var carNames: UILabel!
    @IBOutlet var carsImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
