//
//  RegisterViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var repwdTxtField: UITextField!
    @IBOutlet var pwdTxtfield: UITextField!
    @IBOutlet var locationTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var usernameTxtField: UITextField!
    @IBOutlet var lastnameTxtField: UITextField!
    @IBOutlet var firstnameTxtField: UITextField!
    @IBOutlet var registerBtn: UIButton!
    @IBOutlet var repwdLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var pwdLbl: UILabel!
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var lastNameLbl: UILabel!
    @IBOutlet var firstNameLbl: UILabel!
    @IBOutlet var fillFormLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstnameTxtField.delegate = self
        lastnameTxtField.delegate = self
        usernameTxtField.delegate = self
        emailTxtField.delegate = self
        locationTxtField.delegate = self
        pwdTxtfield.delegate = self
        repwdTxtField.delegate = self
        compatibility()

            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            fillFormLbl.font = UIFont.systemFontOfSize(22)
            firstNameLbl.font = UIFont.systemFontOfSize(18)
            lastNameLbl.font = UIFont.systemFontOfSize(18)
            userNameLbl.font = UIFont.systemFontOfSize(18)
            emailLbl.font = UIFont.systemFontOfSize(18)
            pwdLbl.font = UIFont.systemFontOfSize(18)
            locationLbl.font = UIFont.systemFontOfSize(18)
            repwdLbl.font = UIFont.systemFontOfSize(18)
            registerBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            
            
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
   }
