//
//  LoginViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var usernameView: UIView!
    @IBOutlet var pwdLineView: UIView!
    @IBOutlet var registerBtn: UIButton!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var forgotPwdBtn: UIButton!
    @IBOutlet var passwordLbl: UILabel!
    @IBOutlet var usernameLbl: UILabel!
    @IBOutlet var pwdTxtField: UITextField!
    @IBOutlet var userNameTxtField: UITextField!
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // userNameTxtField.becomeFirstResponder()
        userNameTxtField.delegate = self
        pwdTxtField.delegate = self
        
        compatibility()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func compatibility()
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            usernameLbl.font = UIFont.systemFontOfSize(22)
            passwordLbl.font = UIFont.systemFontOfSize(22)
            forgotPwdBtn.titleLabel?.font = UIFont.systemFontOfSize(22)
            loginBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            registerBtn.titleLabel?.font = UIFont.systemFontOfSize(22)
            usernameView.frame = CGRectMake(usernameView.frame.origin.x, usernameView.frame.origin.y, usernameView.frame.size.width, 3)
            userNameTxtField.font = UIFont.systemFontOfSize(18)
            pwdTxtField.font = UIFont.systemFontOfSize(18)
        }
        
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if sizeHeight == 480
        {
            if textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y -= 60
            }
            
        }
        else if sizeHeight == 568
        {
            if  textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y -= 50
            }
            
            
        }
        else
        {
            
            self.view.frame.origin.y = self.view.frame.origin.y
            
            
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if sizeHeight == 480
        {
            if textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y += 60
                
            }
            
            
        }
            
        else if sizeHeight == 568
        {
            if  textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y += 50
            }
            
            
        }
        else
        {
            
            self.view.frame.origin.y = self.view.frame.origin.y
            
            
        }
        
        
        return true
    }
    
}
