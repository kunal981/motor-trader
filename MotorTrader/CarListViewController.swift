//
//  CarListViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CarListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet var menuView: UIView!
    var carImageArray = NSArray()
    var carNameArray = NSArray()
    var carPriceArray = NSArray()
    var carModelNoArray = NSArray()
    var carTypeArray = NSArray()
    var carLocationArray = NSArray()
    var carModelYearArray = NSArray()
    
    override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationItem.setHidesBackButton(true, animated: false)
    self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255, green: 144.0/255, blue: 15.0/255, alpha: 1.0)
      carImageArray = ["XUV500.jpg","XUV500.jpg","XUV500.jpg","XUV500.jpg","XUV500.jpg","XUV500.jpg"]
      carNameArray = ["XUV500","XUV500","XUV500","XUV500","XUV500","XUV500"]
      carPriceArray = ["1500000","1500000","1500000","1500000","1500000","1500000"]
      carModelNoArray = ["081432536","081432536","081432536","081432536","081432536","081432536"]
      carTypeArray = ["Diesel","Diesel","Diesel","Diesel","Diesel","Diesel"]
      carLocationArray = ["Ipokia","Ipokia","Ipokia","Ipokia","Ipokia","Ipokia"]
      carModelYearArray = ["2010","2010","2010","2010","2010","2010"]
      
        
        //self.menuView.frame = CGRectMake(self.menuView.frame.origin.x - self.view.frame.size.width, menuView.frame.origin.y, menuView.frame.size.width, menuView.frame.size.height)
        
    }
    
    
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
            if self.menuView.frame.origin.x == -355
            {
        
        UIView.animateWithDuration(0.5, animations:{
            
            
            
            self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            
            
        })
        }
                
        else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return carImageArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
       
        var customCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCellCarListTableViewCell
        
        customCell.carsImage.image = UIImage(named: carImageArray[indexPath.row] as! String)
        customCell.carNames.text = carNameArray[indexPath.row] as? String
        
        customCell.carPrice.text = carPriceArray[indexPath.row] as? String
        customCell.carModelNo.text = carModelNoArray[indexPath.row] as? String
        
        customCell.carType.text = carTypeArray[indexPath.row] as? String
        customCell.carLocation.text = carLocationArray[indexPath.row] as? String
        
        customCell.carModelYear.text = carModelYearArray[indexPath.row] as? String
       
        
        
        return customCell
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let carDetail = storyboard?.instantiateViewControllerWithIdentifier("carDetail") as? CarDetailViewController
        
        self.navigationController?.pushViewController(carDetail!, animated: true)
    }
    @IBAction func signOut(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
}
