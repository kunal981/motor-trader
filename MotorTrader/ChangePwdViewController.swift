//
//  ChangePwdViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ChangePwdViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var rePwdTxtField: UITextField!
    @IBOutlet var newPwdTxtField: UITextField!
    @IBOutlet var currentPwdTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPwdTxtField.delegate = self
        newPwdTxtField.delegate = self
        rePwdTxtField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
}
