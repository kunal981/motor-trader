//
//  SparePartViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SparePartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var spareImageArray = NSArray()
    var spareNameArray = NSArray()
    var sparePriceArray = NSArray()
    var spareModelNoArray = NSArray()
    var spareTypeArray = NSArray()
    var spareLocationArray = NSArray()
    var spareModelYearArray = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        spareImageArray = ["sparePart.png","sparePart.png","sparePart.png","sparePart.png","sparePart.png","sparePart.png"]
        spareNameArray = ["Xtreme Fuel Treatment","Xtreme Fuel Treatment","Xtreme Fuel Treatment","Xtreme Fuel Treatment","Xtreme Fuel Treatment","Xtreme Fuel Treatment"]
        sparePriceArray = ["9000","9000","9000","9000","9000","9000"]
        spareModelNoArray = ["081432536","081432536","081432536","081432536","081432536","081432536"]
        spareTypeArray = ["Diesel","Diesel","Diesel","Diesel","Diesel","Diesel"]
        spareLocationArray = ["Ipokia","Ipokia","Ipokia","Ipokia","Ipokia","Ipokia"]
        spareModelYearArray = ["2010","2010","2010","2010","2010","2010"]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return spareImageArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var spareCustomCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! SparePartCustomCell
        spareCustomCell.sparePartImages.image = UIImage(named: spareImageArray[indexPath.row] as! String)
        spareCustomCell.SparePartName.text = spareNameArray[indexPath.row] as? String
        spareCustomCell.sparePartPrice.text = sparePriceArray[indexPath.row] as? String
        spareCustomCell.ModelNo.text = spareModelNoArray[indexPath.row] as? String
        spareCustomCell.partType.text = spareTypeArray[indexPath.row] as? String
        spareCustomCell.location.text = spareLocationArray[indexPath.row] as? String
        spareCustomCell.modelYear.text = spareModelYearArray[indexPath.row] as? String
        
        return spareCustomCell
        
        
    }
   

    
}
