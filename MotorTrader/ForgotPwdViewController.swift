//
//  ForgotPwdViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ForgotPwdViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var txtView: UITextView!
    @IBOutlet var forgotPwdLbl: UILabel!
    @IBOutlet var emailTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
      emailTxtField.delegate = self
        compatibility()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
        forgotPwdLbl.font = UIFont.systemFontOfSize(22)
        txtView.font = UIFont.systemFontOfSize(18)
        emailLbl.font = UIFont.systemFontOfSize(20)
        submitBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
         emailTxtField.font = UIFont.systemFontOfSize(18)
        
        }
    }

    
}
