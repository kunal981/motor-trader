//
//  SparePartCustomCell.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SparePartCustomCell: UITableViewCell {

    @IBOutlet var location: UILabel!
    @IBOutlet var modelYear: UILabel!
    @IBOutlet var partType: UILabel!
    @IBOutlet var ModelNo: UILabel!
    @IBOutlet var sparePartPrice: UILabel!
    @IBOutlet var SparePartName: UILabel!
    @IBOutlet var sparePartImages: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
