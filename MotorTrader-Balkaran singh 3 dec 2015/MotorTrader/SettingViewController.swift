//
//  SettingViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    @IBOutlet var menuView: UIView!
    @IBOutlet var alertBtn: UIButton!
    var sizeHeight = UIScreen.mainScreen().bounds.height
    @IBOutlet var switchObj: UISwitch!
    @IBOutlet var changePwdBtn: UIButton!
    @IBOutlet var aboutBtn: UIButton!
    @IBOutlet var privacyBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Setting"
        
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        menuView.frame = CGRectMake(-355, view.frame.origin.y+60 , menuView.frame.size.width, menuView.frame.size.height)
        
        if sizeHeight == 480
        {
            switchObj.frame = CGRectMake(switchObj.frame.origin.x, switchObj.frame.origin.y - 5, switchObj.frame.size.width, switchObj.frame.size.height)
            
        }
        else if sizeHeight == 568
        {
            switchObj.frame = CGRectMake(switchObj.frame.origin.x, switchObj.frame.origin.y-3 , switchObj.frame.size.width, switchObj.frame.size.height)
            
        }
        else
        {
            switchObj.frame = CGRectMake(switchObj.frame.origin.x, switchObj.frame.origin.y , switchObj.frame.size.width, switchObj.frame.size.height)
            
        }
        
        
        compatibility()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func compatibility()
    {
        
        if  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            self.menuView.frame = CGRectMake(-800, view.frame.origin.y + 60, menuView.frame.size.width, menuView.frame.size.height)
            alertBtn.titleLabel?.font = UIFont.systemFontOfSize(20)
            privacyBtn.titleLabel?.font = UIFont.systemFontOfSize(20)
            aboutBtn.titleLabel?.font = UIFont.systemFontOfSize(20)
            changePwdBtn.titleLabel?.font = UIFont.systemFontOfSize(20)
            
        }
        
        
    }
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }
            
        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
    }
    @IBAction func home(sender: AnyObject)
    {
        
        var carlist = storyboard?.instantiateViewControllerWithIdentifier("carList") as! CarListViewController
        
        self.navigationController?.pushViewController(carlist, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
    }
    
    
    @IBAction func searchCar(sender: AnyObject)
    {
        
        var searchCar = storyboard?.instantiateViewControllerWithIdentifier("searchCar") as! AdvanceSearchViewController
        
        self.navigationController?.pushViewController(searchCar, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
            
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
            
            
        }
    }
    
    
    @IBAction func searchSpareParts(sender: AnyObject)
    {
        var sparePart = storyboard?.instantiateViewControllerWithIdentifier("sparePart") as! SparePartViewController
        
        self.navigationController?.pushViewController(sparePart, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    @IBAction func addCar(sender: AnyObject)
    {
        var addCar = storyboard?.instantiateViewControllerWithIdentifier("addCar") as! AddCarDetailViewController
        
        self.navigationController?.pushViewController(addCar, animated: false)
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func setting(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func addSparePart(sender: AnyObject)
    {
        var addSpare = storyboard?.instantiateViewControllerWithIdentifier("addSpare") as! AddSparePartViewController
        
        self.navigationController?.pushViewController(addSpare, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func signOut(sender: AnyObject)
    {
        
        var login:LoginViewController = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: false)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "login")
        
        
    }
    
    
}
