//
//  SparePartViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SparePartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var loaderView: UIView!
    
    @IBOutlet var sparePartMenuBtn: UIBarButtonItem!
    @IBOutlet var sparePartBarBtn: UIBarButtonItem!
    @IBOutlet var loaderSubView: UIView!
    var loadingMessage = UILabel()
    @IBOutlet var menuView: UIView!
    @IBOutlet var spareTableView: UITableView!
    var data = NSData()
    var mutableData = NSMutableData()
    var sparePartArray = NSMutableArray()
    var imageCache = [String:UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Spare Part List"
        
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        menuView.frame = CGRectMake(-355, view.frame.origin.y+60 , menuView.frame.size.width, menuView.frame.size.height)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            spareTableView.rowHeight = 200
            
            
        }
        
        
        
        compatibility()
        apiData()
    }
    
    func compatibility()
    {
        
        if  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            self.menuView.frame = CGRectMake(-800, view.frame.origin.y + 60, menuView.frame.size.width, menuView.frame.size.height)
        }
    }
    func apiData()
        
    {
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        loadingMessage.text = "Loading"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        
        indicator.startAnimating()
        loaderView.hidden = false
        
        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        var strUserId = userDefaults.valueForKey("userID") as! String
        println(strUserId)
        
        var post = NSString(format: "userid=%@", strUserId)
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        var url = NSURL(string: "http://www.motortrader.ng/api/sparepartslisting.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var connection = NSURLConnection(request: urlRequest, delegate: self)
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.mutableData.appendData(data)
    }
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        var error:NSError?
        
        var spareList = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        println(spareList)
        
        let value = spareList.valueForKey("success") as! Bool
        println(value)
        
        if value
        {
            
            
            sparePartArray = spareList.valueForKey("data") as! NSMutableArray
            
        }
            
        else
            
        {
            let str = spareList.valueForKey("message") as! String
            
            let alertView = UIAlertView(title: "Alert", message:str, delegate: self, cancelButtonTitle: "OK")
            alertView.show()
            
        }
        
        
        
        
        if (error  != nil)
        {
            println("\(error?.localizedDescription)")
        }
        else
        {
            
            
            dispatch_async(dispatch_get_main_queue(),{
                self.spareTableView.reloadData()
                self.loaderView.hidden = true
                
                println(self.sparePartArray)
                
                
            });
            
            
        }
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sparePartArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var spareCustomCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! SparePartCustomCell
        
        spareCustomCell.location.text = ""
        
        if var loc = sparePartArray[indexPath.row].valueForKey("city") as? String
        {
            
            spareCustomCell.location.text = loc
        }
        
        
        spareCustomCell.modelYear.text = ""
        
        if var year = sparePartArray[indexPath.row].valueForKey("year") as? String
        {
            
            
            spareCustomCell.modelYear.text = year
            
        }
        
        spareCustomCell.sparePartPrice.text = ""
        
        if var price = sparePartArray[indexPath.row].valueForKey("price") as? String
        {
            
            
            spareCustomCell.sparePartPrice.text = price
        }
        
        spareCustomCell.SparePartName.text = ""
        
        if var part = sparePartArray[indexPath.row].valueForKey("model") as? String
        {
            
            spareCustomCell.SparePartName.text = part
        }
        
        spareCustomCell.ModelNo.text = ""
        
        if var modelNo = sparePartArray[indexPath.row].valueForKey("seller_num") as? String
        {
            
            spareCustomCell.ModelNo.text = modelNo
        }
        spareCustomCell.partType.text = ""
        
        if var partType = sparePartArray[indexPath.row].valueForKey("company") as? String
        {
            
            
            spareCustomCell.partType.text = partType
            
        }
        if  var urlString  =  sparePartArray[indexPath.row].valueForKey("image") as? String, imagUrl = NSURL(string: urlString)
            
        {
            
            
            spareCustomCell.sparePartImages.image = UIImage(named: "")
            
            
            if var img = imageCache[urlString]
                
                
            {
                
                spareCustomCell.sparePartImages.contentMode = UIViewContentMode.ScaleAspectFit
                spareCustomCell.sparePartImages.image = img
                
                
                
                
                
            }
                
            else
            {
                
                let request : NSURLRequest = NSURLRequest(URL: imagUrl)
                
                
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response,data,error) -> Void in
                    
                    
                    if error == nil
                    {
                        
                        let image = UIImage(data: data)
                        
                        self.imageCache[urlString] = image
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            
                            
                            if   let cellUpdate = tableView.cellForRowAtIndexPath(indexPath) as? SparePartCustomCell
                            {
                                cellUpdate.sparePartImages.image = image
                            }
                            
                            
                            
                        })
                        
                    }
                        
                    else
                    {
                        
                        println("Error: \(error.localizedDescription)")
                        
                    }
                    
                })
                
                
            }
            
        }
        
        
        
        return spareCustomCell
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var sparePartDetail = storyboard?.instantiateViewControllerWithIdentifier("spareDetail") as! SparePartDetailViewController
        
        strSpareId = sparePartArray[indexPath.row].valueForKey("id") as! String
        self.navigationController?.pushViewController(sparePartDetail, animated: true)
        
        
    }
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }
            
        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
    }
    @IBAction func home(sender: AnyObject)
    {
        
        var carlist = storyboard?.instantiateViewControllerWithIdentifier("carList") as! CarListViewController
        
        self.navigationController?.pushViewController(carlist, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
    }
    
    
    @IBAction func searchCar(sender: AnyObject)
    {
        
        var searchCar = storyboard?.instantiateViewControllerWithIdentifier("searchCar") as! AdvanceSearchViewController
        
        self.navigationController?.pushViewController(searchCar, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
            
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
            
            
        }
    }
    
    
    @IBAction func searchSpareParts(sender: AnyObject)
    {
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    @IBAction func addCar(sender: AnyObject)
    {
        var addCar = storyboard?.instantiateViewControllerWithIdentifier("addCar") as! AddCarDetailViewController
        
        self.navigationController?.pushViewController(addCar, animated: false)
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func setting(sender: AnyObject)
    {
        
        var setting = storyboard?.instantiateViewControllerWithIdentifier("setting") as! SettingViewController
        
        self.navigationController?.pushViewController(setting, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func addSparePart(sender: AnyObject)
    {
        var addSpare = storyboard?.instantiateViewControllerWithIdentifier("addSpare") as! AddSparePartViewController
        
        self.navigationController?.pushViewController(addSpare, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func signOut(sender: AnyObject)
    {
        
        var login:LoginViewController = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: false)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "login")
        
        
    }
    
    
}
