//
//  CarImagesViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/26/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

//var carArray = NSArray()

class CarImagesViewController: UIViewController {

    @IBOutlet var scrollViewImage: UIScrollView!
    
    var currentIndex: Int!
    
    var imageArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        println(currentIndex)
     
       
            
            createScoll()
            
            
            

        
        
        
        
    }
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(false)
        
    }

    
    
    func createScoll()
    
    {
        scrollViewImage.contentSize.width = CGFloat(imageArray.count) * scrollViewImage.frame.width
        
        for (var i = 0; i < imageArray.count; i++ )
            
            
        {
            
          
            
            var image1 = UIImageView()
            
            image1.frame = CGRectMake(CGFloat(i) * self.scrollViewImage.frame.width, 0, self.scrollViewImage.frame.width, self.scrollViewImage.frame.height)
                
                image1.transform = CGAffineTransformMakeScale(1.0, 1.0)
            
            
            image1.image = self.imageArray[i] as? UIImage
            
            image1.contentMode = UIViewContentMode.ScaleAspectFit
            
            self.scrollViewImage.addSubview(image1)
                
                
            
        }
        
        scrollViewImage.contentOffset.x = CGFloat(currentIndex) * scrollViewImage.frame.width
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

    @IBAction func pinchImage(sender: UIPinchGestureRecognizer)
    {
        
        if let view2 = sender.view
        {
            view2.transform = CGAffineTransformScale(view2.transform, sender.scale, sender.scale)
            
            sender.scale = 1
        }

        
        
        
    }
    
}
