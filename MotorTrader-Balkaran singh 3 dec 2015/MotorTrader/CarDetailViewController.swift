//
//  CarDetailViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

var carIdStr = NSString()
class CarDetailViewController: UIViewController,UIScrollViewDelegate {
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var loadingMessage = UILabel()
    var data = NSData()
    var mutableData = NSMutableData()
    var dictCarDetail=NSMutableDictionary()
    @IBOutlet var carPrice: UILabel!
    
    @IBOutlet var writeToUsBtn: UIButton!
    @IBOutlet var sellerLbl: UILabel!
    @IBOutlet var loaderView: UIView!
    @IBOutlet var loaderSubView: UIView!
    
    @IBOutlet var carImage: UIImageView!
    @IBOutlet var carName: UILabel!
    
    @IBOutlet var sellerNo: UILabel!
    @IBOutlet var bodyValueLbl: UILabel!
    @IBOutlet var transmissionValueLbl: UILabel!
    @IBOutlet var fuelValueLbl: UILabel!
    @IBOutlet var locationValueLbl: UILabel!
    @IBOutlet var yearValueLbl: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var bodyLbl: UILabel!
    @IBOutlet var transmissionLbl: UILabel!
    @IBOutlet var fuelLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var yearLbl: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var imageScroll: UIScrollView!
    var imageArray = NSMutableArray()
    var index:Int = Int()
   

    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        
             
        
        self.navigationItem.title = "Car Detail"
        
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        self.navigationController?.navigationBarHidden = false
        
        
        
        
        carDetailApi()
        println(carIdStr)
        compatibility()
    }
    
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            carPrice.font = UIFont.systemFontOfSize(20)
            yearLbl.font = UIFont.systemFontOfSize(18)
            locationLbl.font = UIFont.systemFontOfSize(18)
            fuelLbl.font = UIFont.systemFontOfSize(18)
            transmissionLbl.font = UIFont.systemFontOfSize(18)
            bodyLbl.font = UIFont.systemFontOfSize(18)
            descriptionLbl.font = UIFont.systemFontOfSize(16)
            yearValueLbl.font = UIFont.systemFontOfSize(18)
            locationValueLbl.font = UIFont.systemFontOfSize(18)
            fuelValueLbl.font = UIFont.systemFontOfSize(18)
            transmissionValueLbl.font = UIFont.systemFontOfSize(18)
            bodyValueLbl.font = UIFont.systemFontOfSize(18)
            sellerLbl.font = UIFont.systemFontOfSize(14)
            sellerNo.font = UIFont.systemFontOfSize(14)
            
            carName.font = UIFont.boldSystemFontOfSize(22)
            
            writeToUsBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
        }
    }
    
    func carDetailApi()
    {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        loadingMessage.text = "Loading"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        
        indicator.startAnimating()
        loaderView.hidden = false
        
        
        var post = NSString(format: "car_id=%@", carIdStr)
        
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        var url = NSURL(string: "http://www.motortrader.ng/api/carDetails.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var connection = NSURLConnection(request: urlRequest, delegate: self)
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.mutableData.appendData(data)
    }
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        var error:NSError?
        
        var carDetailData = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        
        self.dictCarDetail = carDetailData.valueForKey("data") as! NSMutableDictionary
        
        println(dictCarDetail)
        
        
        
        if error != nil
        {
            println("\(error?.localizedDescription)")
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),{
                
                self.loaderView.hidden = true
                self.carPrice.text = self.dictCarDetail.valueForKey("price") as? String
                self.sellerNo.text = self.dictCarDetail.valueForKey("seller_num") as? String
                
                var str1 = ""
                
                var str2 = ""
                
                if var make = self.dictCarDetail.valueForKey("make") as? String
                {
                    str1 = make
                }
                if var model = self.dictCarDetail.valueForKey("model") as? String
                {
                    str2 = model
                    
                }
                var str3 =  NSString(format: "%@ %@", str1,str2) as? String
                println(str3)
                
                self.carName.text = (str3)
                self.yearValueLbl.text = self.dictCarDetail.valueForKey("year") as? String
                
                var strlocation1 = ""
                var strlocation2 = ""
                
                if var location = self.dictCarDetail.valueForKey("location") as? String
                {
                    strlocation1 = location
                }
                if var state = self.dictCarDetail.valueForKey("state") as? String
                {
                    strlocation2 = state
                    
                }
                
                var strlocation3 =  NSString(format: "%@ (%@)", strlocation1,strlocation2) as? String
                
                
                println(strlocation3)
                
                self.locationValueLbl.text = (strlocation3)
                
                self.fuelValueLbl.text = ""
                
                if var fuelValue = self.dictCarDetail.valueForKey("fuel_type") as? String
                {
                    
                    self.fuelValueLbl.text = fuelValue
                    
                }
                
                self.transmissionValueLbl.text = ""
                if var trans = self.dictCarDetail.valueForKey("transmission") as? String
                {
                    
                    
                    self.transmissionValueLbl.text = trans
                }
                
                self.bodyValueLbl.text = ""
                
                if var body = self.dictCarDetail.valueForKey("body_type") as? String
                {
                    
                    self.bodyValueLbl.text = body
                }
                
                
                self.textView.text = ""
                
                if var txt = self.dictCarDetail.valueForKey("desc") as? String
                {
                    
                    self.textView.text = txt
                    
                    let contentSize = self.textView.sizeThatFits(self.textView.bounds.size)
                    var frame = self.textView.frame
                    frame.size.height = contentSize.height
                    self.textView.frame = frame
                    
                    
                    self.scrollView.contentSize.height = self.scrollView.frame.height + self.textView.frame.height
                    
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
                    {
                        self.textView.font = UIFont.systemFontOfSize(16)
                        
                    }
                    
                    
                    
                }
                
                
                if  var url = self.dictCarDetail.valueForKey("image") as? String,imagUrl = NSURL(string: url.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                        self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }
                
                
                if  var url2 = self.dictCarDetail.valueForKey("image2") as? String,imagUrl = NSURL(string: url2.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                        self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }
                

                if  var url3 = self.dictCarDetail.valueForKey("image3") as? String,imagUrl = NSURL(string: url3.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                        self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }
                
                if  var url4 = self.dictCarDetail.valueForKey("image4") as? String,imagUrl = NSURL(string: url4.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                        self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }

                
                println(self.imageArray.count)
                
              
                
                
                self.imageScroll.contentSize.width = CGFloat(self.imageArray.count) * self.imageScroll.frame.width
                
               
                
                for (  self.index = 0; self.index < self.imageArray.count;self.index++)
                {
                    var image = UIImageView()
                    
                    image.frame = CGRectMake(CGFloat(self.index)*self.imageScroll.frame.width, 0, self.imageScroll.frame.width, self.imageScroll.frame.height)
                    
                    
                    
                    image.image = self.imageArray[self.index] as? UIImage
                    
                    image.contentMode = UIViewContentMode.ScaleAspectFit
                    println(self.imageArray[self.index])
                    
                    var imageBtn = UIButton()
                    imageBtn = UIButton.buttonWithType(UIButtonType.System) as! UIButton
                    imageBtn.setTitle("", forState: UIControlState.Normal)
                    imageBtn.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
                    
                    
                    imageBtn.frame = image.frame
                    
                    imageBtn.tag = self.index
                    
                    self.imageScroll.addSubview(image)
                    self.imageScroll.addSubview(imageBtn)
                    
                    
                }
                
              
               
                
            });
            
            
            
            
        }
        
        
        
        
    }
    
    func buttonAction(sender:AnyObject)
    {
      
            
        
        let carImages = self.storyboard?.instantiateViewControllerWithIdentifier("carImages") as! CarImagesViewController
        
        carImages.currentIndex = sender.tag
        
        println(sender.tag)
        
        carImages.imageArray = self.imageArray
        
        
        
        self.navigationController?.pushViewController(carImages, animated: false)
    
    }
    
}
