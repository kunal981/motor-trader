//
//  AddCarDetailViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/11/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AddCarDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    @IBOutlet var menuView: UIView!
    
   
    @IBOutlet var descriptionTxtField: UITextField!
    @IBOutlet var priceTxtField: UITextField!
    var makedata = NSMutableData()
    var makeListingArray = NSMutableArray()
    
    var modelData = NSData()
    var modelListingArray = NSMutableArray()
    
    var bodydata = NSMutableData()
    var bodyListingArray = NSMutableArray()
    
    
    var strTransmission = NSString()
    var strFuel = NSString()
    var strBody = NSString()
    var strMake = NSString()
    var strModel = NSString()
    
   
    @IBOutlet var carPictureLbl: UILabel!
    @IBOutlet var sellerLbl: UILabel!
    @IBOutlet var carDetailLbl: UILabel!
    @IBOutlet var nextBtn: UIButton!
    
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var bodyLbl: UILabel!
    @IBOutlet var fuelTypeLbl: UILabel!
    @IBOutlet var transmissionLbl: UILabel!
    @IBOutlet var yearLbl: UILabel!
    @IBOutlet var modelLbl: UILabel!
    @IBOutlet var makelbl: UILabel!
    
    
    
    @IBOutlet var bodyTypeTableView: UITableView!
    @IBOutlet var bodyTypeBtn: UIButton!
    @IBOutlet var fuelTypeTableView: UITableView!
    @IBOutlet var fuelTypeBtn: UIButton!
    @IBOutlet var transmissionTableView: UITableView!
    @IBOutlet var transmissionBtn: UIButton!
    @IBOutlet var yearTableView: UITableView!
    @IBOutlet var yearBtn: UIButton!
    @IBOutlet var modelTableView: UITableView!
    @IBOutlet var modelBtn: UIButton!
    @IBOutlet var makebtn: UIButton!
    @IBOutlet var makeTableView: UITableView!
    
    @IBOutlet var makeImage: UIImageView!
    @IBOutlet var bodyImage: UIImageView!
    @IBOutlet var fuelImage: UIImageView!
    @IBOutlet var transmissionImage: UIImageView!
    @IBOutlet var yearImage: UIImageView!
    @IBOutlet var modelImage: UIImageView!
    
    
    @IBOutlet var addCarStateLbl: UILabel!
    
    @IBOutlet var addCarStateBtn: UIButton!
    
    @IBOutlet var addCarStateImage: UIImageView!
    
    @IBOutlet var addCarStateTableView: UITableView!
    
    var locationListingArray = NSMutableArray()
    var yearArray = NSArray()
    var transmissionArray = NSArray()
    var fuelTypeArray = NSArray()
    
    var strLocation = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Add Car"
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        priceTxtField.delegate = self
        descriptionTxtField.delegate = self
        menuView.frame = CGRectMake(-355, view.frame.origin.y+60 , menuView.frame.size.width, menuView.frame.size.height)
        
        addCarStateTableView.frame = CGRectMake(addCarStateBtn.frame.origin.x, addCarStateBtn.frame.origin.y, addCarStateBtn.frame.size.width, 130)
        addCarStateTableView.layer.borderWidth = 1
        
        
        
        
        makeTableView.frame = CGRectMake(makebtn.frame.origin.x, makebtn.frame.origin.y, makebtn.frame.size.width, 150)
        makeTableView.layer.borderWidth = 1
        
        modelTableView.frame = CGRectMake(modelBtn.frame.origin.x, modelBtn.frame.origin.y, modelBtn.frame.size.width, 150)
        modelTableView.layer.borderWidth = 1
        
        yearTableView.frame = CGRectMake(yearBtn.frame.origin.x, yearBtn.frame.origin.y, yearBtn.frame.size.width,150)
        yearTableView.layer.borderWidth = 1
        
        
        transmissionTableView.frame = CGRectMake(transmissionBtn.frame.origin.x, transmissionBtn.frame.origin.y, transmissionBtn.frame.size.width, 150)
        transmissionTableView.layer.borderWidth = 1
        
        fuelTypeTableView.frame = CGRectMake(fuelTypeBtn.frame.origin.x, fuelTypeBtn.frame.origin.y, fuelTypeBtn.frame.size.width, 150)
        fuelTypeTableView.layer.borderWidth = 1
        
        bodyTypeTableView.frame = CGRectMake(bodyTypeBtn.frame.origin.x, bodyTypeBtn.frame.origin.y, bodyTypeBtn.frame.size.width, 150)
        bodyTypeTableView.layer.borderWidth = 1
        
        
        
        yearArray = ["2015","2014","2013","2012","2011","2014","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997",
            "1996", "1995", "1994", "1993",
            "1992"]
        
        transmissionArray = ["Any Transmission","Automatic","Manual","Other"]
        fuelTypeArray = ["Any Transmission","Petrol","Diesel","Hybrid","Other"]
        
        
        
        compatibility()
        bodyApiData()
        makeApiData()
        locationApiData()
    }
    func locationApiData()
    {
        
        var urlBody = NSURL(string: "https://www.motortrader.ng/api/locationlist.php")
        
        var urlRequest = NSURLRequest(URL: urlBody!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.addCarStateTableView.reloadData()
                    
                    
                    
                    self.locationListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.locationListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
        
        
        
        
        
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y -= 180
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y -= 220
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 568
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y -= 160
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y -= 200
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 667
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y -= 150
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y -= 200
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y -= 150
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y -= 200
            }
                
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y += 180
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y += 220
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        else if sizeHeight == 568
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y += 160
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y += 200
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
            
        else if sizeHeight == 667
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y += 150
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y += 200
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == priceTxtField
            {
                self.view.frame.origin.y += 150
                
            }
            else if textField == descriptionTxtField
            {
                self.view.frame.origin.y += 200
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func modelApiData(makeid:NSString)
    {
        
        
        
        println(makeid)
        
        var post = NSString(format: "makeId=%@", makeid)
        
        modelData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(modelData.length)
        
        var url = NSURL(string: "http://www.motortrader.ng/api/findModel.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = modelData
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.modelTableView.reloadData()
                    
                    
                    
                    self.modelListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.bodyListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
            
            
            
            
            
            
            
            
        });
        task.resume()
        
        
    }
    
    func bodyApiData()
    {
        
        var urlBody = NSURL(string: "https://www.motortrader.ng/api/bodylist.php")
        
        var urlRequest = NSURLRequest(URL: urlBody!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.bodyTypeTableView.reloadData()
                    
                    
                    
                    self.bodyListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.bodyListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
        
    }
    
    func makeApiData()
    {
        
        
        var urlMake = NSURL(string: "http://www.motortrader.ng/api/findmake.php")
        
        var urlRequest = NSURLRequest(URL: urlMake!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.makeTableView.reloadData()
                    
                  
                    
                    self.makeListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.makeListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
    }
    
    
    func compatibility ()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            self.menuView.frame = CGRectMake(-800, view.frame.origin.y + 60, menuView.frame.size.width, menuView.frame.size.height)
            
            carDetailLbl.font = UIFont.systemFontOfSize(14)
            carPictureLbl.font = UIFont.systemFontOfSize(14)
            sellerLbl.font = UIFont.systemFontOfSize(14)
            
            makelbl.font = UIFont.systemFontOfSize(18)
            modelLbl.font = UIFont.systemFontOfSize(18)
            yearLbl.font = UIFont.systemFontOfSize(18)
            transmissionLbl.font = UIFont.systemFontOfSize(18)
            fuelTypeLbl.font = UIFont.systemFontOfSize(18)
            bodyLbl.font = UIFont.systemFontOfSize(18)
            priceLbl.font = UIFont.systemFontOfSize(18)
            descriptionLbl.font = UIFont.systemFontOfSize(18)
            addCarStateLbl.font = UIFont.systemFontOfSize(18)
            
            nextBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
            makeImage.frame = CGRectMake(makeImage.frame.origin.x + 3, makeImage.frame.origin.y - 3 , makeImage.frame.size.width, makeImage.frame.size.height)
            
            addCarStateImage.frame = CGRectMake(addCarStateImage.frame.origin.x + 3, addCarStateImage.frame.origin.y - 3 , addCarStateImage.frame.size.width, addCarStateImage.frame.size.height)
            
            
            modelImage.frame = CGRectMake(modelImage.frame.origin.x + 3, modelImage.frame.origin.y - 3 , modelImage.frame.size.width, modelImage.frame.size.height)
            
            yearImage.frame = CGRectMake(yearImage.frame.origin.x + 3, yearImage.frame.origin.y - 3 , yearImage.frame.size.width, yearImage.frame.size.height)
            
            transmissionImage.frame = CGRectMake(transmissionImage.frame.origin.x + 3, transmissionImage.frame.origin.y - 3 , transmissionImage.frame.size.width, transmissionImage.frame.size.height)
            
            fuelImage.frame = CGRectMake(fuelImage.frame.origin.x + 3, fuelImage.frame.origin.y - 3 , fuelImage.frame.size.width, fuelImage.frame.size.height)
            
            bodyImage.frame = CGRectMake(bodyImage.frame.origin.x + 3, bodyImage.frame.origin.y - 3 , bodyImage.frame.size.width, bodyImage.frame.size.height)
            
            
            addCarStateTableView.frame = CGRectMake(addCarStateBtn.frame.origin.x, addCarStateBtn.frame.origin.y, addCarStateBtn.frame.size.width, 230)
            
            
            
            
            makeTableView.frame = CGRectMake(makebtn.frame.origin.x, makebtn.frame.origin.y, makebtn.frame.size.width, 230)
            
            modelTableView.frame = CGRectMake(modelBtn.frame.origin.x, modelBtn.frame.origin.y, modelBtn.frame.size.width, 230)
            
            yearTableView.frame = CGRectMake(yearBtn.frame.origin.x, yearBtn.frame.origin.y, yearBtn.frame.size.width,230)
            
            
            transmissionTableView.frame = CGRectMake(transmissionBtn.frame.origin.x, transmissionBtn.frame.origin.y, transmissionBtn.frame.size.width, 230)
            
            fuelTypeTableView.frame = CGRectMake(fuelTypeBtn.frame.origin.x, fuelTypeBtn.frame.origin.y, fuelTypeBtn.frame.size.width, 230)
            
            bodyTypeTableView.frame = CGRectMake(bodyTypeBtn.frame.origin.x, bodyTypeBtn.frame.origin.y, bodyTypeBtn.frame.size.width, 230)
            
            
            
            
            
        }
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == makeTableView
        {
            return makeListingArray.count
        }
        else if tableView == modelTableView
        {
            return modelListingArray.count
        }
        else if tableView == yearTableView
        {
            
            return yearArray.count
            
        }
        else if tableView == transmissionTableView
        {
            return transmissionArray.count
            
        }
        else if tableView == fuelTypeTableView
        {
            return fuelTypeArray.count
            
        }
        else if tableView == bodyTypeTableView
        {
            return bodyListingArray.count
        }
        else
        {
            return locationListingArray.count
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        
        if tableView == makeTableView
        {
            var makeCell = tableView.dequeueReusableCellWithIdentifier("makeCell", forIndexPath: indexPath) as! UITableViewCell
            makeCell.textLabel?.text = ""
            
            if var make = makeListingArray[indexPath.row].valueForKey("name") as? String
            {
                makeCell.textLabel?.text = make
            }
            makeCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return makeCell
            
            
        }
        else if tableView == modelTableView
        {
            var modelCell = tableView.dequeueReusableCellWithIdentifier("modelCell", forIndexPath: indexPath) as! UITableViewCell
            modelCell.textLabel?.text = ""
            if var model = modelListingArray[indexPath.row].valueForKey("model") as? String
            {
                modelCell.textLabel?.text = model
            }
            modelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return modelCell
        }
        else if tableView == yearTableView
        {
            
            var yearCell = tableView.dequeueReusableCellWithIdentifier("yearCell", forIndexPath: indexPath) as! UITableViewCell
            
            yearCell.textLabel?.text = yearArray[indexPath.row] as? String
            yearCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return yearCell
            
        }
        else if tableView == transmissionTableView
        {
            var transmissionCell = tableView.dequeueReusableCellWithIdentifier("transmissionCell", forIndexPath: indexPath) as! UITableViewCell
            
            transmissionCell.textLabel?.text = transmissionArray[indexPath.row] as? String
            transmissionCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return transmissionCell
            
        }
        else if tableView == fuelTypeTableView
        {
            var fuelCell = tableView.dequeueReusableCellWithIdentifier("fuelCell", forIndexPath: indexPath) as! UITableViewCell
            
            fuelCell.textLabel?.text = fuelTypeArray[indexPath.row] as? String
            fuelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return fuelCell
            
        }
        else if tableView == bodyTypeTableView
        {
            var bodyCell = tableView.dequeueReusableCellWithIdentifier("bodyCell", forIndexPath: indexPath) as! UITableViewCell
            bodyCell.textLabel?.text = ""
            if var body = bodyListingArray[indexPath.row].valueForKey("name") as? String
            {
                bodyCell.textLabel?.text = body
            }
            bodyCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return bodyCell
        }
        else
        {
            var locationCell = tableView.dequeueReusableCellWithIdentifier("stateCell", forIndexPath: indexPath) as! UITableViewCell
            locationCell.textLabel?.text = ""
            
            if var loc = locationListingArray[indexPath.row].valueForKey("name") as? String
            {
                locationCell.textLabel?.text = loc
                
            }
            locationCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return locationCell
        }
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == makeTableView
        {
            
            
            
            modelApiData((makeListingArray[indexPath.row].valueForKey("id") as? String)!)
            
            strMake = makeListingArray[indexPath.row].valueForKey("id") as! String
            
            makebtn.setTitle(makeListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            makeTableView.hidden = true
            
            
        }
        else if tableView == modelTableView
        {
            
            strModel = modelListingArray[indexPath.row].valueForKey("id") as! String
            
            modelBtn.setTitle(modelListingArray[indexPath.row].valueForKey("model") as? String, forState: UIControlState.Normal)
            modelTableView.hidden = true
            
        }
        else if tableView == yearTableView
        {
            yearBtn.setTitle(yearArray[indexPath.row] as? String, forState: UIControlState.Normal)
            yearTableView.hidden = true
            
            
        }
        else if tableView == transmissionTableView
        {
            
            strTransmission = NSString(format: "%i",indexPath.row)
            
            println(strTransmission)
            
            transmissionBtn.setTitle(transmissionArray[indexPath.row] as? String, forState: UIControlState.Normal)
            transmissionTableView.hidden = true
            
            
        }
        else if tableView == fuelTypeTableView
        {
            strFuel = NSString(format: "%i",indexPath.row)
            println(strFuel)
            
            fuelTypeBtn.setTitle(fuelTypeArray[indexPath.row] as? String, forState: UIControlState.Normal)
            fuelTypeTableView.hidden = true
            
        }
        else if tableView == bodyTypeTableView
        {
            
            strBody = bodyListingArray[indexPath.row].valueForKey("id") as! String
            
            bodyTypeBtn.setTitle(bodyListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            bodyTypeTableView.hidden = true
            
        }
        else
            
        {
            strLocation = (locationListingArray[indexPath.row].valueForKey("id") as? String)!
            
            addCarStateBtn.setTitle(locationListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            addCarStateTableView.hidden = true
        }
        
    }
    
    
    @IBAction func makeBtn(sender: AnyObject)
    {
        makeTableView.reloadData()
        makeTableView.hidden = false
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        addCarStateTableView.hidden = true
    }
    
    @IBAction func modelBtn(sender: AnyObject)
    {
        modelTableView.reloadData()
        makeTableView.hidden = true
        modelTableView.hidden = false
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        addCarStateTableView.hidden = true
    }
    
    @IBAction func yearBtn(sender: AnyObject)
    {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = false
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        addCarStateTableView.hidden = true
        
    }
    
    @IBAction func transmissionBtn(sender: AnyObject)
    {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = false
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        addCarStateTableView.hidden = true
    }
    
    @IBAction func fuelTypeBtn(sender: AnyObject)
    {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = false
        bodyTypeTableView.hidden = true
        addCarStateTableView.hidden = true
    }
    
    
    @IBAction func bodyTypeBtn(sender: AnyObject)
    {
        bodyTypeTableView.reloadData()
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = false
        addCarStateTableView.hidden = true
    }
    
    @IBAction func addCarstatebtn(sender: AnyObject)
    {
        
        addCarStateTableView.reloadData()
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        addCarStateTableView.hidden = false
        
    }
    
    
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        priceTxtField.resignFirstResponder()
        descriptionTxtField.resignFirstResponder()
        addCarStateTableView.hidden = true
    }
    
    @IBAction func nextBtn(sender: AnyObject)
    {
        if makebtn.titleLabel?.text == "Any Make" || modelBtn.titleLabel?.text == "Any Model" || yearBtn.titleLabel?.text == "Any Year" || transmissionBtn.titleLabel?.text == "Any Transmission" || fuelTypeBtn.titleLabel?.text == "Any Fuel" || bodyTypeBtn.titleLabel?.text == "Any Body" || priceTxtField.text == "" || descriptionTxtField.text == ""
            
        {
            let alert = UIAlertView(title: "Alert", message: "Select all fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
            
        else
        {
            
                
                
            
            
            
            let addCarPicture = self.storyboard?.instantiateViewControllerWithIdentifier("addCarPicture") as! AddCarPictureViewController
            
            
            strMakeBtn = self.strMake
            strModelBtn = self.strModel
            strYearBtn = self.yearBtn.titleLabel!.text!
            strTransmissionBtn = self.strTransmission
            strFuelBtn = self.strFuel
            strBodyBtn = self.strBody
            strPriceTxt = self.priceTxtField.text
            strDescriptionTxt = self.descriptionTxtField.text
            strAddCarState = self.strLocation
            println(strMakeBtn)
            println(strModelBtn)
            println(strYearBtn)
            println(strTransmissionBtn)
            println(strFuelBtn)
            println(strBodyBtn)
            println(strPriceTxt)
            println(strDescriptionTxt)
            println(strAddCarState)

            
            self.navigationController?.pushViewController(addCarPicture, animated: true)
                
                
               
        }
        
    }
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }
            
        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
    }
    @IBAction func home(sender: AnyObject)
    {
        
        var carlist = storyboard?.instantiateViewControllerWithIdentifier("carList") as! CarListViewController
        
        self.navigationController?.pushViewController(carlist, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
    }
    
    
    @IBAction func searchCar(sender: AnyObject)
    {
        
        var searchCar = storyboard?.instantiateViewControllerWithIdentifier("searchCar") as! AdvanceSearchViewController
        
        self.navigationController?.pushViewController(searchCar, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
            
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
            
            
        }
    }
    
    
    @IBAction func searchSpareParts(sender: AnyObject)
    {
        var sparePart = storyboard?.instantiateViewControllerWithIdentifier("sparePart") as! SparePartViewController
        
        self.navigationController?.pushViewController(sparePart, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    @IBAction func addCar(sender: AnyObject)
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func setting(sender: AnyObject)
    {
        var setting = storyboard?.instantiateViewControllerWithIdentifier("setting") as! SettingViewController
        
        self.navigationController?.pushViewController(setting, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func addSparePart(sender: AnyObject)
    {
        var addSpare = storyboard?.instantiateViewControllerWithIdentifier("addSpare") as! AddSparePartViewController
        
        self.navigationController?.pushViewController(addSpare, animated: false)
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func signOut(sender: AnyObject)
    {
        
        var login:LoginViewController = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: false)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "login")
        
        
    }
    
}
