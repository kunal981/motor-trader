//
//  AdvanceSearchViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/12/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AdvanceSearchViewController: UIViewController {
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var subView = UIView()
    var loadingMessage = UILabel()
    
    @IBOutlet var loaderSubView: UIView!
    @IBOutlet var loaderView: UIView!
    
    
    @IBOutlet var newLocationLbl: UILabel!
    @IBOutlet var menuView: UIView!
    var makedata = NSMutableData()
    var makeListingArray = NSMutableArray()
    
    @IBOutlet var qualityLocationLbl: UILabel!
    
    var locationListingArray = NSMutableArray()
    
    
    var searchdata = NSData()
    @IBOutlet var qualityLocationImage: UIImageView!
    var bodydata = NSMutableData()
    var bodyListingArray = NSMutableArray()
    
    
    @IBOutlet var qualityLocationBtn: UIButton!
    
    @IBOutlet var qualityLocationTableView: UITableView!
    var modelData = NSData()
    var modelListingArray = NSMutableArray()
    
    var strLocation = NSString()
    var strTransmission = NSString()
    var strFuel = NSString()
    var strBody = NSString()
    
    
    var strNewMake = NSString()
    var strNewModel = NSString()
    
    var strQulaityMake = NSString()
    var strQualityModel = NSString()
    
    
    @IBOutlet var newCarLocationImage: UIImageView!
    @IBOutlet var newCarLocationBtn: UIButton!
    @IBOutlet var newCarLocationtableView: UITableView!
    @IBOutlet var qualityBodyBtn: UIButton!
    @IBOutlet var qualityFuelBtn: UIButton!
    @IBOutlet var qualityTransmissionBtn: UIButton!
    @IBOutlet var qualityYearBtn: UIButton!
    @IBOutlet var qualityModelBtn: UIButton!
    @IBOutlet var qualityMakeBtn: UIButton!
    @IBOutlet var qualityFuelTableView: UITableView!
    @IBOutlet var qualityModelTableView: UITableView!
    @IBOutlet var qualitytransmissionTableView: UITableView!
    @IBOutlet var qualityBodyTableView: UITableView!
    @IBOutlet var qualityYearTableView: UITableView!
    @IBOutlet var qualitymakeTableView: UITableView!
    @IBOutlet var bodyLbl: UILabel!
    @IBOutlet var fuelTypeLbl: UILabel!
    @IBOutlet var transmissionLbl: UILabel!
    @IBOutlet var yearLbl: UILabel!
    @IBOutlet var modelLbl: UILabel!
    @IBOutlet var makelbl: UILabel!
    
    @IBOutlet var searchBtn: UIButton!
    
    @IBOutlet var makeImage: UIImageView!
    @IBOutlet var bodyImage: UIImageView!
    @IBOutlet var fuelImage: UIImageView!
    @IBOutlet var transmissionImage: UIImageView!
    @IBOutlet var yearImage: UIImageView!
    @IBOutlet var modelImage: UIImageView!
    
    @IBOutlet var btnsView: UIView!
    @IBOutlet var qualityUsedBtn: UIButton!
    
    @IBOutlet var newCarBtn: UIButton!
    @IBOutlet var lineView: UIView!
    @IBOutlet var qualityView: UIView!
    @IBOutlet var bodyTypeTableView: UITableView!
    @IBOutlet var bodyTypeBtn: UIButton!
    @IBOutlet var fuelTypeTableView: UITableView!
    @IBOutlet var fuelTypeBtn: UIButton!
    @IBOutlet var transmissionTableView: UITableView!
    @IBOutlet var transmissionBtn: UIButton!
    @IBOutlet var yearTableView: UITableView!
    @IBOutlet var yearBtn: UIButton!
    @IBOutlet var modelTableView: UITableView!
    @IBOutlet var modelBtn: UIButton!
    @IBOutlet var makebtn: UIButton!
    @IBOutlet var makeTableView: UITableView!
    
    @IBOutlet var newCarMakeImage: UIImageView!
    
    @IBOutlet var newCarBodyImage: UIImageView!
    @IBOutlet var newCarFuelImage: UIImageView!
    @IBOutlet var newCarTransmissionImage: UIImageView!
    @IBOutlet var newCarYearImage: UIImageView!
    @IBOutlet var newCarModelImage: UIImageView!
    @IBOutlet var newCarMakelbl: UILabel!
    
    @IBOutlet var newCarModelLbl: UILabel!
    
    @IBOutlet var newCarYearLbl: UILabel!
    
    @IBOutlet var newCarTransmissionLbl: UILabel!
    @IBOutlet var newCarFuelLbl: UILabel!
    
    @IBOutlet var newCarBodyLbl: UILabel!
    
    
    var yearArray = NSArray()
    var transmissionArray = NSArray()
    var fuelTypeArray = NSArray()
    
    
    @IBOutlet var newCarSearchBtn: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Search Car"
        
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        menuView.frame = CGRectMake(-355, view.frame.origin.y+60 , menuView.frame.size.width, menuView.frame.size.height)
        
        qualityLocationTableView.frame = CGRectMake(qualityLocationBtn.frame.origin.x, qualityLocationBtn.frame.origin.y,qualityLocationBtn.frame.size.width , 150)
        qualityLocationTableView.layer.borderWidth = 1
        
        newCarLocationtableView.frame = CGRectMake(newCarLocationBtn.frame.origin.x, newCarLocationBtn.frame.origin.y, newCarLocationBtn.frame.size.width, 150)
        newCarLocationtableView.layer.borderWidth = 1
        
        makeTableView.frame = CGRectMake(makebtn.frame.origin.x, makebtn.frame.origin.y, makebtn.frame.size.width, 150)
        makeTableView.layer.borderWidth = 1
        
        
        modelTableView.frame = CGRectMake(modelBtn.frame.origin.x, modelBtn.frame.origin.y, modelBtn.frame.size.width, 150)
        modelTableView.layer.borderWidth = 1
        
        yearTableView.frame = CGRectMake(yearBtn.frame.origin.x, yearBtn.frame.origin.y, yearBtn.frame.size.width, 150)
        yearTableView.layer.borderWidth = 1
        
        transmissionTableView.frame = CGRectMake(transmissionBtn.frame.origin.x, transmissionBtn.frame.origin.y, transmissionBtn.frame.size.width, 150)
        transmissionTableView.layer.borderWidth = 1
        
        fuelTypeTableView.frame = CGRectMake(fuelTypeBtn.frame.origin.x, fuelTypeBtn.frame.origin.y, fuelTypeBtn.frame.size.width, 150)
        fuelTypeTableView.layer.borderWidth = 1
        
        bodyTypeTableView.frame = CGRectMake(bodyTypeBtn.frame.origin.x, bodyTypeBtn.frame.origin.y, bodyTypeBtn.frame.size.width, 150)
        bodyTypeTableView.layer.borderWidth = 1
        
        btnsView.frame = CGRectMake(self.btnsView.frame.origin.x, self.view.frame.origin.y + 60,self.btnsView.frame.size.width , self.btnsView.frame.size.height)
        
        
        
        qualitymakeTableView.frame = CGRectMake(qualityMakeBtn.frame.origin.x, qualityMakeBtn.frame.origin.y, qualityMakeBtn.frame.size.width, 150)
        
        qualitymakeTableView.layer.borderWidth = 1
        
        qualityModelTableView.frame = CGRectMake(qualityModelBtn.frame.origin.x, qualityModelBtn.frame.origin.y, qualityModelBtn.frame.size.width,150)
        qualityModelTableView.layer.borderWidth = 1
        
        qualityYearTableView.frame = CGRectMake(qualityYearBtn.frame.origin.x, qualityYearBtn.frame.origin.y, qualityYearBtn.frame.size.width, 150)
        qualityYearTableView.layer.borderWidth = 1
        
        qualitytransmissionTableView.frame = CGRectMake(qualityTransmissionBtn.frame.origin.x, qualityTransmissionBtn.frame.origin.y, qualityTransmissionBtn.frame.size.width, 150)
        qualitytransmissionTableView.layer.borderWidth = 1
        
        
        qualityFuelTableView.frame = CGRectMake(qualityFuelBtn.frame.origin.x, qualityFuelBtn.frame.origin.y, qualityFuelBtn.frame.size.width, 150)
        qualityFuelTableView.layer.borderWidth = 1
        
        qualityBodyTableView.frame = CGRectMake(qualityBodyBtn.frame.origin.x, qualityBodyBtn.frame.origin.y, qualityBodyBtn.frame.size.width, 150)
        
        qualityBodyTableView.layer.borderWidth = 1
        
        
        
        
        
        
        
        
        
        yearArray = ["2015","2014","2013","2012","2011","2014","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997",
            "1996", "1995", "1994", "1993",
            "1992"]
        
        transmissionArray = ["Any Transmission","Automatic","Manual","Other"]
        fuelTypeArray = ["Any Transmission","Petrol","Diesel","Hybrid","Other"]
        
        locationApiData()
        bodyApiData()
        makeApiData()
        compatibility()
    }
    
    
    
    func compatibility ()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            newLocationLbl.font = UIFont.systemFontOfSize(18)
            qualityLocationLbl.font = UIFont.systemFontOfSize(18)
            
            makelbl.font = UIFont.systemFontOfSize(18)
            modelLbl.font = UIFont.systemFontOfSize(18)
            yearLbl.font = UIFont.systemFontOfSize(18)
            transmissionLbl.font = UIFont.systemFontOfSize(18)
            fuelTypeLbl.font = UIFont.systemFontOfSize(18)
            bodyLbl.font = UIFont.systemFontOfSize(18)
            
            self.menuView.frame = CGRectMake(-800, view.frame.origin.y + 60, menuView.frame.size.width, menuView.frame.size.height)
            searchBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
            makeImage.frame = CGRectMake(makeImage.frame.origin.x + 3, makeImage.frame.origin.y - 3 , makeImage.frame.size.width, makeImage.frame.size.height)
            
            modelImage.frame = CGRectMake(modelImage.frame.origin.x + 3, modelImage.frame.origin.y - 3 , modelImage.frame.size.width, modelImage.frame.size.height)
            
            yearImage.frame = CGRectMake(yearImage.frame.origin.x + 3, yearImage.frame.origin.y - 3 , yearImage.frame.size.width, yearImage.frame.size.height)
            
            transmissionImage.frame = CGRectMake(transmissionImage.frame.origin.x + 3, transmissionImage.frame.origin.y - 3 , transmissionImage.frame.size.width, transmissionImage.frame.size.height)
            
            fuelImage.frame = CGRectMake(fuelImage.frame.origin.x + 3, fuelImage.frame.origin.y - 3 , fuelImage.frame.size.width, fuelImage.frame.size.height)
            
            bodyImage.frame = CGRectMake(bodyImage.frame.origin.x + 3, bodyImage.frame.origin.y - 3 , bodyImage.frame.size.width, bodyImage.frame.size.height)
            
            newCarLocationImage.frame = CGRectMake(newCarLocationImage.frame.origin.x + 3, newCarLocationImage.frame.origin.y - 3 , newCarLocationImage.frame.size.width, newCarLocationImage.frame.size.height)
            
            qualityLocationImage.frame = CGRectMake(qualityLocationImage.frame.origin.x + 3, qualityLocationImage.frame.origin.y - 3 , qualityLocationImage.frame.size.width, qualityLocationImage.frame.size.height)
            
            newCarBtn.titleLabel?.font = UIFont.systemFontOfSize(20)
            qualityUsedBtn.titleLabel?.font = UIFont.systemFontOfSize(20)
            
            
            newCarMakelbl.font = UIFont.systemFontOfSize(18)
            newCarModelLbl.font = UIFont.systemFontOfSize(18)
            newCarYearLbl.font = UIFont.systemFontOfSize(18)
            newCarTransmissionLbl.font = UIFont.systemFontOfSize(18)
            newCarFuelLbl.font = UIFont.systemFontOfSize(18)
            newCarBodyLbl.font = UIFont.systemFontOfSize(18)
            
            
            newCarSearchBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
            newCarMakeImage.frame = CGRectMake(newCarMakeImage.frame.origin.x + 3, newCarMakeImage.frame.origin.y - 3 , newCarMakeImage.frame.size.width, newCarMakeImage.frame.size.height)
            
            newCarModelImage.frame = CGRectMake(newCarModelImage.frame.origin.x + 3, newCarModelImage.frame.origin.y - 3 , newCarModelImage.frame.size.width, newCarModelImage.frame.size.height)
            
            newCarYearImage.frame = CGRectMake(newCarYearImage.frame.origin.x + 3, newCarYearImage.frame.origin.y - 3 , newCarYearImage.frame.size.width, newCarYearImage.frame.size.height)
            
            newCarTransmissionImage.frame = CGRectMake(newCarTransmissionImage.frame.origin.x + 3, newCarTransmissionImage.frame.origin.y - 3 , newCarTransmissionImage.frame.size.width, newCarTransmissionImage.frame.size.height)
            
            newCarFuelImage.frame = CGRectMake(newCarFuelImage.frame.origin.x + 3, newCarFuelImage.frame.origin.y - 3 , newCarFuelImage.frame.size.width, newCarFuelImage.frame.size.height)
            
            newCarBodyImage.frame = CGRectMake(newCarBodyImage.frame.origin.x + 3, newCarBodyImage.frame.origin.y - 3 , newCarBodyImage.frame.size.width, newCarBodyImage.frame.size.height)
            
            
            
            qualityLocationTableView.frame = CGRectMake(qualityLocationBtn.frame.origin.x, qualityLocationBtn.frame.origin.y,qualityLocationBtn.frame.size.width , 250)
            
            
            newCarLocationtableView.frame = CGRectMake(newCarLocationBtn.frame.origin.x, newCarLocationBtn.frame.origin.y, newCarLocationBtn.frame.size.width, 250)
            
            
            makeTableView.frame = CGRectMake(makebtn.frame.origin.x, makebtn.frame.origin.y, makebtn.frame.size.width, 250)
            
            
            
            modelTableView.frame = CGRectMake(modelBtn.frame.origin.x, modelBtn.frame.origin.y, modelBtn.frame.size.width, 250)
            
            
            yearTableView.frame = CGRectMake(yearBtn.frame.origin.x, yearBtn.frame.origin.y, yearBtn.frame.size.width, 250)
            
            
            transmissionTableView.frame = CGRectMake(transmissionBtn.frame.origin.x, transmissionBtn.frame.origin.y, transmissionBtn.frame.size.width, 250)
            
            
            fuelTypeTableView.frame = CGRectMake(fuelTypeBtn.frame.origin.x, fuelTypeBtn.frame.origin.y, fuelTypeBtn.frame.size.width, 250)
            
            
            bodyTypeTableView.frame = CGRectMake(bodyTypeBtn.frame.origin.x, bodyTypeBtn.frame.origin.y, bodyTypeBtn.frame.size.width, 250)
            
            qualitymakeTableView.frame = CGRectMake(qualityMakeBtn.frame.origin.x, qualityMakeBtn.frame.origin.y, qualityMakeBtn.frame.size.width, 250)
            
            
            
            qualityModelTableView.frame = CGRectMake(qualityModelBtn.frame.origin.x, qualityModelBtn.frame.origin.y, qualityModelBtn.frame.size.width,250)
            
            
            qualityYearTableView.frame = CGRectMake(qualityYearBtn.frame.origin.x, qualityYearBtn.frame.origin.y, qualityYearBtn.frame.size.width, 250)
            
            
            qualitytransmissionTableView.frame = CGRectMake(qualityTransmissionBtn.frame.origin.x, qualityTransmissionBtn.frame.origin.y, qualityTransmissionBtn.frame.size.width, 250)
            
            
            
            qualityFuelTableView.frame = CGRectMake(qualityFuelBtn.frame.origin.x, qualityFuelBtn.frame.origin.y, qualityFuelBtn.frame.size.width, 250)
            
            
            qualityBodyTableView.frame = CGRectMake(qualityBodyBtn.frame.origin.x, qualityBodyBtn.frame.origin.y, qualityBodyBtn.frame.size.width, 250)
            
            
            
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    func locationApiData()
    {
        
        var urlBody = NSURL(string: "https://www.motortrader.ng/api/locationlist.php")
        
        var urlRequest = NSURLRequest(URL: urlBody!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.newCarLocationtableView.reloadData()
                    self.qualityLocationTableView.reloadData()
                    
                    
                    self.locationListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.locationListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
        
        
        
        
        
        
    }
    
    
    func modelApiData(makeid:NSString)
    {
        
        println(makeid)
        
        var post = NSString(format: "makeId=%@", makeid)
        
        modelData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(modelData.length)
        
        var url = NSURL(string: "http://www.motortrader.ng/api/findModel.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = modelData
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.modelTableView.reloadData()
                    self.qualityModelTableView.reloadData()
                    
                    
                    self.modelListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.bodyListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
            
            
            
            
            
            
            
            
        });
        task.resume()
        
        
    }
    
    
    func bodyApiData()
    {
        
        var urlBody = NSURL(string: "https://www.motortrader.ng/api/bodylist.php")
        
        var urlRequest = NSURLRequest(URL: urlBody!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.bodyTypeTableView.reloadData()
                    self.qualityBodyTableView.reloadData()
                    
                    
                    self.bodyListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.bodyListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
        
    }
    
    func makeApiData()
    {
        
        
        var urlMake = NSURL(string: "http://www.motortrader.ng/api/findmake.php")
        
        var urlRequest = NSURLRequest(URL: urlMake!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.makeTableView.reloadData()
                    self.qualitymakeTableView.reloadData()
                    
                    
                    self.makeListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.makeListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == newCarLocationtableView
        {
            return locationListingArray.count
            
        }
            
        else if tableView == makeTableView
        {
            return makeListingArray.count
        }
        else if tableView == modelTableView
        {
            return modelListingArray.count
        }
        else if tableView == yearTableView
        {
            
            return yearArray.count
            
        }
        else if tableView == transmissionTableView
        {
            return transmissionArray.count
            
        }
        else if tableView == fuelTypeTableView
        {
            return fuelTypeArray.count
            
        }
        else if  tableView == bodyTypeTableView
        {
            return bodyListingArray.count
        }
        if tableView == qualitymakeTableView
        {
            return makeListingArray.count
        }
        else if tableView == qualityModelTableView
        {
            return modelListingArray.count
        }
        else if tableView == qualityYearTableView
        {
            
            return yearArray.count
            
        }
        else if tableView == qualitytransmissionTableView
        {
            return transmissionArray.count
            
        }
        else if tableView == qualityFuelTableView
        {
            return fuelTypeArray.count
            
        }
            
        else if tableView == qualityLocationTableView
        {
            return locationListingArray.count
            
        }
        else
        {
            return bodyListingArray.count
        }
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        if tableView == newCarLocationtableView
        {
            
            
            var locationCell = tableView.dequeueReusableCellWithIdentifier("locationCell", forIndexPath: indexPath) as! UITableViewCell
            
            locationCell.textLabel?.text = ""
            if var loclbl = locationListingArray[indexPath.row].valueForKey("name") as? String
            {
                
                locationCell.textLabel?.text = loclbl
                
            }
            
            locationCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return locationCell
        }
            
        else if tableView == makeTableView
        {
            var makeCell = tableView.dequeueReusableCellWithIdentifier("makeCell", forIndexPath: indexPath) as! UITableViewCell
            
            makeCell.textLabel?.text = ""
            
            if var makelbl = makeListingArray[indexPath.row].valueForKey("name") as? String
            {
                
                makeCell.textLabel?.text = makelbl
            }
            makeCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return makeCell
            
            
        }
        else if tableView == modelTableView
        {
            var modelCell = tableView.dequeueReusableCellWithIdentifier("modelCell", forIndexPath: indexPath) as! UITableViewCell
            
            modelCell.textLabel?.text = ""
            
            if var modelLbl = modelListingArray[indexPath.row].valueForKey("model") as? String
            {
                
                modelCell.textLabel?.text = modelLbl
            }
            modelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return modelCell
        }
        else if tableView == yearTableView
        {
            
            var yearCell = tableView.dequeueReusableCellWithIdentifier("yearCell", forIndexPath: indexPath) as! UITableViewCell
            
            yearCell.textLabel?.text = yearArray[indexPath.row] as? String
            yearCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return yearCell
            
        }
        else if tableView == transmissionTableView
        {
            var transmissionCell = tableView.dequeueReusableCellWithIdentifier("transmissionCell", forIndexPath: indexPath) as! UITableViewCell
            
            transmissionCell.textLabel?.text = transmissionArray[indexPath.row] as? String
            transmissionCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return transmissionCell
            
        }
        else if tableView == fuelTypeTableView
        {
            var fuelCell = tableView.dequeueReusableCellWithIdentifier("fuelCell", forIndexPath: indexPath) as! UITableViewCell
            
            fuelCell.textLabel?.text = fuelTypeArray[indexPath.row] as? String
            fuelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return fuelCell
            
        }
        else if tableView == bodyTypeTableView
        {
            var bodyCell = tableView.dequeueReusableCellWithIdentifier("bodyCell", forIndexPath: indexPath) as! UITableViewCell
            bodyCell.textLabel?.text = ""
            
            if var body = bodyListingArray[indexPath.row].valueForKey("name") as? String
            {
                bodyCell.textLabel?.text = body
            }
            bodyCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return bodyCell
        }
        else if tableView == qualitymakeTableView
        {
            var makeCell = tableView.dequeueReusableCellWithIdentifier("makeCell", forIndexPath: indexPath) as! UITableViewCell
            makeCell.textLabel?.text = ""
            
            if var make = makeListingArray[indexPath.row].valueForKey("name") as? String
            {
                makeCell.textLabel?.text = make
            }
            makeCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return makeCell
            
            
        }
        else if tableView == qualityModelTableView
        {
            var modelCell = tableView.dequeueReusableCellWithIdentifier("modelCell", forIndexPath: indexPath) as! UITableViewCell
            modelCell.textLabel?.text = ""
            
            if var model = modelListingArray[indexPath.row].valueForKey("model") as? String
            {
                
                modelCell.textLabel?.text = model
            }
            modelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return modelCell
        }
        else if tableView == qualityYearTableView
        {
            
            var yearCell = tableView.dequeueReusableCellWithIdentifier("yearCell", forIndexPath: indexPath) as! UITableViewCell
            
            yearCell.textLabel?.text = yearArray[indexPath.row] as? String
            yearCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return yearCell
            
        }
        else if tableView == qualitytransmissionTableView
        {
            var transmissionCell = tableView.dequeueReusableCellWithIdentifier("transmissionCell", forIndexPath: indexPath) as! UITableViewCell
            
            transmissionCell.textLabel?.text = transmissionArray[indexPath.row] as? String
            transmissionCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return transmissionCell
            
        }
        else if tableView == qualityFuelTableView
        {
            var fuelCell = tableView.dequeueReusableCellWithIdentifier("fuelCell", forIndexPath: indexPath) as! UITableViewCell
            
            fuelCell.textLabel?.text = fuelTypeArray[indexPath.row] as? String
            fuelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return fuelCell
            
        }
            
            
        else if tableView == qualityLocationTableView
        {
            
            var locationCell = tableView.dequeueReusableCellWithIdentifier("locationCell", forIndexPath: indexPath) as! UITableViewCell
            locationCell.textLabel?.text = ""
            
            if var loc = locationListingArray[indexPath.row].valueForKey("name") as? String
            {
                locationCell.textLabel?.text = loc
            }
            locationCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return locationCell
            
        }
            
        else
        {
            var bodyCell = tableView.dequeueReusableCellWithIdentifier("bodyCell", forIndexPath: indexPath) as! UITableViewCell
            bodyCell.textLabel?.text = ""
            
            if var body = bodyListingArray[indexPath.row].valueForKey("name") as? String
            {
                bodyCell.textLabel?.text = body
            }
            bodyCell.textLabel?.font = UIFont.systemFontOfSize(12)
            return bodyCell
        }
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
        if tableView == newCarLocationtableView
        {
            strLocation = (locationListingArray[indexPath.row].valueForKey("id") as? String)!
            
            println(strLocation)
            
            newCarLocationBtn.setTitle(locationListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            newCarLocationtableView.hidden = true
            
        }
        else  if tableView == makeTableView
        {
            modelApiData((makeListingArray[indexPath.row].valueForKey("id") as? String)!)
            
            
            strNewMake = makeListingArray[indexPath.row].valueForKey("id") as! String
            makebtn.setTitle(makeListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            makeTableView.hidden = true
            
            
        }
        else if tableView == modelTableView
        {
            
            strNewModel = modelListingArray[indexPath.row].valueForKey("id") as! String
            modelBtn.setTitle(modelListingArray[indexPath.row].valueForKey("model") as? String, forState: UIControlState.Normal)
            modelTableView.hidden = true
            
        }
        else if tableView == yearTableView
        {
            yearBtn.setTitle(yearArray[indexPath.row] as? String, forState: UIControlState.Normal)
            yearTableView.hidden = true
            
            
        }
        else if tableView == transmissionTableView
        {
            
            
            
            strTransmission = NSString(format: "%i",indexPath.row)
            
            
            
            println(strTransmission)
            
            transmissionBtn.setTitle(transmissionArray[indexPath.row] as? String, forState: UIControlState.Normal)
            
            
            transmissionTableView.hidden = true
            
            
        }
        else if tableView == fuelTypeTableView
        {
            
            strFuel = NSString(format: "%i",indexPath.row)
            println(strFuel)
            
            
            fuelTypeBtn.setTitle(fuelTypeArray[indexPath.row] as? String, forState: UIControlState.Normal)
            fuelTypeTableView.hidden = true
            
        }
        else if tableView == bodyTypeTableView
        {
            strBody = (bodyListingArray[indexPath.row].valueForKey("id") as? String)!
            
            println(strBody)
            bodyTypeBtn.setTitle(bodyListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            bodyTypeTableView.hidden = true
            
        }
            
            
            
        else if tableView == qualitymakeTableView
        {
            modelApiData((makeListingArray[indexPath.row].valueForKey("id") as? String)!)
            
            
            strQulaityMake = makeListingArray[indexPath.row].valueForKey("id") as! String
            
            qualityMakeBtn.setTitle(makeListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            qualitymakeTableView.hidden = true
            
            
        }
        else if tableView == qualityModelTableView
        {
            
            strQualityModel = modelListingArray[indexPath.row].valueForKey("id") as! String
            
            qualityModelBtn.setTitle(modelListingArray[indexPath.row].valueForKey("model") as? String, forState: UIControlState.Normal)
            qualityModelTableView.hidden = true
            
        }
        else if tableView == qualityYearTableView
        {
            qualityYearBtn.setTitle(yearArray[indexPath.row] as? String, forState: UIControlState.Normal)
            qualityYearTableView.hidden = true
            
            
        }
        else if tableView == qualitytransmissionTableView
        {
            
            
            strTransmission = NSString(format: "%i",indexPath.row)
            
            
            
            println(strTransmission)
            qualityTransmissionBtn.setTitle(transmissionArray[indexPath.row] as? String, forState: UIControlState.Normal)
            qualitytransmissionTableView.hidden = true
            
            
        }
        else if tableView == qualityFuelTableView
        {
            
            strFuel = NSString(format: "%i",indexPath.row)
            println(strFuel)
            qualityFuelBtn.setTitle(fuelTypeArray[indexPath.row] as? String, forState: UIControlState.Normal)
            qualityFuelTableView.hidden = true
            
        }
            
        else if tableView == qualityLocationTableView
        {
            
            strLocation = (locationListingArray[indexPath.row].valueForKey("id") as? String)!
            
            println(strLocation)
            qualityLocationBtn.setTitle(locationListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            qualityLocationTableView.hidden = true
            
        }
        else
        {
            strBody = (bodyListingArray[indexPath.row].valueForKey("id") as? String)!
            println(strBody)
            
            qualityBodyBtn.setTitle(bodyListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            qualityBodyTableView.hidden = true
            
        }
        
    }
    
    
    @IBAction func makeBtn(sender: AnyObject)
    {
        self.makeTableView.reloadData()
        makeTableView.hidden = false
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        newCarLocationtableView.hidden = true
    }
    
    @IBAction func modelBtn(sender: AnyObject)
    {
        modelTableView.reloadData()
        makeTableView.hidden = true
        modelTableView.hidden = false
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        newCarLocationtableView.hidden = true
    }
    
    @IBAction func yearBtn(sender: AnyObject)
    {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = false
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        newCarLocationtableView.hidden = true
        
    }
    
    @IBAction func transmissionBtn(sender: AnyObject)
    {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = false
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        newCarLocationtableView.hidden = true
    }
    
    @IBAction func fuelTypeBtn(sender: AnyObject)
    {
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = false
        bodyTypeTableView.hidden = true
        newCarLocationtableView.hidden = true
    }
    
    
    @IBAction func bodyTypeBtn(sender: AnyObject)
    {
        bodyTypeTableView.reloadData()
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = false
        newCarLocationtableView.hidden = true
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        newCarLocationtableView.hidden = true
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = true
    }
    
    @IBAction func newCarBtn(sender: AnyObject)
    {
        UIView.animateWithDuration(0.1, animations:{
            
            
            
            self.qualityView.frame = CGRectMake(self.view.frame.size.width,self.qualityView.frame.origin.y , self.qualityView.frame.size.width ,self.qualityView.frame.size.height)
            
            self.lineView.frame = CGRectMake(self.newCarBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height)
            
            
        })
        
        
    }
    
    @IBAction func qualityLoactionBtn(sender: AnyObject)
    {
        qualityLocationTableView.reloadData()
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = false
    }
    
    
    
    @IBAction func newCarLocationBtn(sender: AnyObject)
    {
        newCarLocationtableView.reloadData()
        makeTableView.hidden = true
        modelTableView.hidden = true
        yearTableView.hidden = true
        transmissionTableView.hidden = true
        fuelTypeTableView.hidden = true
        bodyTypeTableView.hidden = true
        newCarLocationtableView.hidden = false
        
    }
    
    @IBAction func QualityUsedBtn(sender: AnyObject)
    {
        
        if sizeHeight == 480
        {
            
            UIView.animateWithDuration(0.1, animations:{
                
                
                
                self.qualityView.frame = CGRectMake(self.view.frame.origin.x,self.makebtn.frame.origin.y , self.qualityView.frame.size.width ,self.qualityView.frame.size.height)
                
                
                self.lineView.frame = CGRectMake(self.qualityUsedBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height)
                
                
            })
            
            
            
            
        }
        else if sizeHeight == 568
            
        {
            
            UIView.animateWithDuration(0.1, animations:{
                
                
                
                self.qualityView.frame = CGRectMake(self.view.frame.origin.x,self.makebtn.frame.origin.y , self.qualityView.frame.size.width ,self.qualityView.frame.size.height)
                
                
                self.lineView.frame = CGRectMake(self.qualityUsedBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height)
                
                
            })
            
            
            
        }
            
            
        else
        {
            
            
            UIView.animateWithDuration(0.1, animations:{
                
                
                
                self.qualityView.frame = CGRectMake(self.view.frame.origin.x,self.qualityView.frame.origin.y , self.qualityView.frame.size.width ,self.qualityView.frame.size.height)
                
                
                self.lineView.frame = CGRectMake(self.qualityUsedBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height)
                
                
            })
        }
        
    }
    @IBAction func qualitySearchBtn(sender: AnyObject)
    {
        if qualityMakeBtn.titleLabel?.text == "Any Make" || qualityModelBtn.titleLabel?.text == "Any Model" || qualityYearBtn.titleLabel?.text == "Any Year" || qualityTransmissionBtn.titleLabel?.text == "Any Transmission" || qualityFuelBtn.titleLabel?.text == "Any Fuel" || qualityBodyBtn.titleLabel?.text == "Any Body" || qualityLocationBtn.titleLabel?.text == "Any Location"
            
        {
            let alert = UIAlertView(title: "Alert", message: "Select All Fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
        }
        else
        {
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            
            var post = NSString(format: "make=%@&model=%@&year=%@&location=%@&fuelType=%@&transmison=%@&bodyType=%@", strQulaityMake,strQualityModel,qualityYearBtn.titleLabel!.text!,strLocation,strFuel,strTransmission,strBody)
            
            searchdata = post.dataUsingEncoding(NSASCIIStringEncoding)!
            var postLength = String(searchdata.length)
            
            var url = NSURL(string: "http://www.motortrader.ng/api/carSearch.php")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = searchdata
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: { data,response,error -> Void in
                
                
                
                
                
                var error:NSError?
                
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
                
                
                advanceSearchDict = dicObj
                
                self.loaderView.hidden  = true
                let advanceSerach = self.storyboard?.instantiateViewControllerWithIdentifier("advanceSearchTableView") as! AdvanceSearchTableViewController
                
                self.navigationController?.pushViewController(advanceSerach, animated: true)
                
                
                
                
            });
            task.resume()
            
            
        }
        
    }
    
    
    @IBAction func newCarSearchBtn(sender: AnyObject)
    {
        if makebtn.titleLabel?.text == "Any Make" || modelBtn.titleLabel?.text == "Any Model" || yearBtn.titleLabel?.text == "Any Year" || transmissionBtn.titleLabel?.text == "Any Transmission" || fuelTypeBtn.titleLabel?.text == "Any Fuel" || bodyTypeBtn.titleLabel?.text == "Any Body" || newCarLocationBtn.titleLabel?.text == "Any Location"
            
        {
            let alert = UIAlertView(title: "Alert", message: "Select All Fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
        }
        else
        {
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            
            
            var post = NSString(format: "make=%@&model=%@&year=%@&location=%@&fuelType=%@&transmison=%@&bodyType=%@", strNewMake,strNewModel,yearBtn.titleLabel!.text!,strLocation,strFuel,strTransmission,strBody)
            println(post)
            searchdata = post.dataUsingEncoding(NSASCIIStringEncoding)!
            var postLength = String(searchdata.length)
            
            var url = NSURL(string: "http://www.motortrader.ng/api/carSearch.php")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = searchdata
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: { data,response,error -> Void in
                
                
                
                
                
                var error:NSError?
                
                let dc = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
                advanceSearchDict = dc
                
                self.loaderView.hidden  = true
                let advanceSerach = self.storyboard?.instantiateViewControllerWithIdentifier("advanceSearchTableView") as! AdvanceSearchTableViewController
                
                self.navigationController?.pushViewController(advanceSerach, animated: true)
                
                
            });
            task.resume()
            
            
        }
    }
    
    @IBAction func qualityMakeBtn(sender: AnyObject)
    {
        qualitymakeTableView.reloadData()
        qualitymakeTableView.hidden = false
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = true
    }
    
    @IBAction func qualityModelBtn(sender: AnyObject)
    {
        qualityModelTableView.reloadData()
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = false
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = true
    }
    
    @IBAction func qualityYearBtn(sender: AnyObject)
    {
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = false
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = true
        
    }
    
    @IBAction func qualityransmissionBtn(sender: AnyObject)
    {
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = false
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = true
        
    }
    
    
    @IBAction func qualityFuelBtn(sender: AnyObject)
    {
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = false
        qualityBodyTableView.hidden = true
        qualityLocationTableView.hidden = true
        
        
    }
    
    
    @IBAction func qualityBodyBtn(sender: AnyObject)
    {
        qualityBodyTableView.reloadData()
        qualitymakeTableView.hidden = true
        qualityModelTableView.hidden = true
        qualityYearTableView.hidden = true
        qualitytransmissionTableView.hidden = true
        qualityFuelTableView.hidden = true
        qualityBodyTableView.hidden = false
        qualityLocationTableView.hidden = true
        
    }
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }
            
        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
    }
    @IBAction func home(sender: AnyObject)
    {
        
        var carlist = storyboard?.instantiateViewControllerWithIdentifier("carList") as! CarListViewController
        
        self.navigationController?.pushViewController(carlist, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
    }
    
    
    @IBAction func searchCar(sender: AnyObject)
    {
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
            
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
            
            
        }
    }
    
    
    @IBAction func searchSpareParts(sender: AnyObject)
    {
        var sparePart = storyboard?.instantiateViewControllerWithIdentifier("sparePart") as! SparePartViewController
        
        self.navigationController?.pushViewController(sparePart, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    @IBAction func addCar(sender: AnyObject)
    {
        var addCar = storyboard?.instantiateViewControllerWithIdentifier("addCar") as! AddCarDetailViewController
        
        self.navigationController?.pushViewController(addCar, animated: false)
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func setting(sender: AnyObject)
    {
        var setting = storyboard?.instantiateViewControllerWithIdentifier("setting") as! SettingViewController
        
        self.navigationController?.pushViewController(setting, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func addSparePart(sender: AnyObject)
    {
        var addSpare = storyboard?.instantiateViewControllerWithIdentifier("addSpare") as! AddSparePartViewController
        
        self.navigationController?.pushViewController(addSpare, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func signOut(sender: AnyObject)
    {
        
        var login:LoginViewController = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: false)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "login")
        
        
    }
    
}
