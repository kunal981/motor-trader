//
//  AdvanceSearchTableViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/18/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
var advanceSearchDict = NSDictionary()
class AdvanceSearchTableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var advanceSearchArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println("..........\(advanceSearchDict)")
        
        let value = advanceSearchDict.valueForKey("success") as! Bool
        
        if value
        {
            advanceSearchArray = advanceSearchDict.valueForKey("data") as! NSMutableArray
        }
        else
        {
            let str = advanceSearchDict.valueForKey("message") as! String
            let alert = UIAlertView(title: "Alert", message: str, delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return advanceSearchArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var advanceSearchCell = tableView.dequeueReusableCellWithIdentifier("advanceCell", forIndexPath: indexPath) as! AdvanceSearchTableViewCell
        
        var str1 = ""
        var str2 = ""
        
        if var make = advanceSearchArray[indexPath.row].valueForKey("make") as? String
        {
            str1 = make
        }
        
        if var model = advanceSearchArray[indexPath.row].valueForKey("model") as? String
        {
            str2 = model
        }
        
        var str3 =  NSString(format: "%@ %@", str1,str2) as? String
        println(str3)
        
        
        
        advanceSearchCell.searchMakeLbl.text = str3
        
        advanceSearchCell.searchPriceLbl.text = ""
        
        if var price = advanceSearchArray[indexPath.row].valueForKey("price") as? String
        {
            
            advanceSearchCell.searchPriceLbl.text = price
        }
        
        advanceSearchCell.searchSellerLbl.text = ""
        
        if var seller = advanceSearchArray[indexPath.row].valueForKey("number") as? String
        {
            advanceSearchCell.searchSellerLbl.text = seller
        }
        
        advanceSearchCell.searchLocationLbl.text = ""
        
        if var loc = advanceSearchArray[indexPath.row].valueForKey("city") as? String
        {
            
            advanceSearchCell.searchLocationLbl.text = loc
        }
        
        
        advanceSearchCell.seachYearLbl.text = ""
        
        if var year = advanceSearchArray[indexPath.row].valueForKey("year") as? String
        {
            
            advanceSearchCell.seachYearLbl.text = year
        }
        advanceSearchCell.searchFuelLbl.text = ""
        
        if var fuel = advanceSearchArray[indexPath.row].valueForKey("fuelType") as? String
        {
            
            advanceSearchCell.searchFuelLbl.text = fuel
        }
        
        if  var url = NSURL(string: advanceSearchArray[indexPath.row].valueForKey("image") as! String)
            
        {
            
            if var data = NSData(contentsOfURL: url)
            {
                advanceSearchCell.searchImageView.contentMode = UIViewContentMode.ScaleAspectFit
                advanceSearchCell.searchImageView.image = UIImage(data: data)
            }
            
            
        }
        
        
        
        
        return advanceSearchCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        carIdStr = ""
        
        if var strId = advanceSearchArray[indexPath.row].valueForKey("id") as? String
        {
            carIdStr = strId
        }
        
        var cardetail = storyboard?.instantiateViewControllerWithIdentifier("carDetail") as! CarDetailViewController
        self.navigationController?.pushViewController(cardetail, animated: true)
        
    }
    
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)    }
    
}
