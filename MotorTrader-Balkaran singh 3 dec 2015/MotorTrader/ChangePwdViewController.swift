//
//  ChangePwdViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ChangePwdViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate {
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var data = NSData()
    
    @IBOutlet var loaderSubView: UIView!
    @IBOutlet var loaderView: UIView!
    
    var pwdArray = NSMutableArray()
    var alertSuccess = UIAlertView()
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var rePwdLbl: UILabel!
    @IBOutlet var newPwdLbl: UILabel!
    @IBOutlet var currentPwdLbl: UILabel!
    @IBOutlet var changePedLbl: UILabel!
    @IBOutlet var rePwdTxtField: UITextField!
    @IBOutlet var newPwdTxtField: UITextField!
    @IBOutlet var currentPwdTxtField: UITextField!
    
    var loadingMessage  = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPwdTxtField.delegate = self
        newPwdTxtField.delegate = self
        rePwdTxtField.delegate = self
        compatibility()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        currentPwdTxtField.resignFirstResponder()
        newPwdTxtField.resignFirstResponder()
        rePwdTxtField.resignFirstResponder()
    }
    
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            changePedLbl.font = UIFont.systemFontOfSize(20)
            currentPwdLbl.font = UIFont.systemFontOfSize(18)
            newPwdLbl.font = UIFont.systemFontOfSize(18)
            rePwdLbl.font = UIFont.systemFontOfSize(18)
            submitBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == rePwdTxtField
            {
                self.view.frame.origin.y -= 30
                
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == rePwdTxtField
            {
                self.view.frame.origin.y += 30
                
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        
        return true
    }
    
    @IBAction func submitBtn(sender: AnyObject)
    {
        
        if currentPwdTxtField.text == "" || newPwdTxtField.text == "" || rePwdTxtField.text == ""
        {
            let alert = UIAlertView(title: "Alert", message: "Required fields missing", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
            
        else if count(newPwdTxtField.text!) <= 7
        {
            let alert = UIAlertView(title: "Alert", message: "password length too short", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
        else if  newPwdTxtField.text != rePwdTxtField.text
            
        {
            let alert = UIAlertView(title: "Alert", message: "Incorrect Re-password", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
            
        else
            
        {
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            let userDefaults = NSUserDefaults.standardUserDefaults()
            let userid = userDefaults.valueForKey("userID") as! String
            
            var post = NSString(format: "uid=%@&currentpsw=%@&newpass=%@", userid,currentPwdTxtField.text,newPwdTxtField.text)
            
            data = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(data.length)
            
            var url = NSURL(string: "https://www.motortrader.ng/api/resetpassword.php")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = data
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: { data,response,error -> Void in
                
                var error:NSError?
                
                var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
                println(dicObj)
                self.loaderView.hidden = true
                let str = dicObj.valueForKey("message") as! String
                
                self.alertSuccess = UIAlertView(title: "Alert", message: str, delegate: self, cancelButtonTitle: "OK")
                self.alertSuccess.show()
                
                
                
            });
            task.resume()
        }
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
}
