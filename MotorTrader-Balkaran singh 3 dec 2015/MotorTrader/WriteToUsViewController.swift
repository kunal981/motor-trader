//
//  WriteToUsViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class WriteToUsViewController: UIViewController,UITextFieldDelegate {
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var data = NSData()
    
    
    @IBOutlet var loaderView: UIView!
    
    
    @IBOutlet var loaderSubView: UIView!
    
    var mutableData = NSMutableData()
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var messageLbl: UILabel!
    @IBOutlet var phoneLbl: UILabel!
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var enquiryLbl: UILabel!
    @IBOutlet var messageTxtField: UITextField!
    @IBOutlet var phoneTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var nameTxtField: UITextField!
    
    var loadingMessage = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Enquiry Form"
        nameTxtField.delegate = self
        emailTxtField.delegate = self
        phoneTxtField.delegate = self
        messageTxtField.delegate = self
        compatibility()
        
        println(carIdStr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func compatibility ()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            nameLbl.font = UIFont.systemFontOfSize(18)
            emailLbl.font = UIFont.systemFontOfSize(18)
            phoneLbl.font = UIFont.systemFontOfSize(18)
            messageLbl.font = UIFont.systemFontOfSize(18)
            
            enquiryLbl.font = UIFont.boldSystemFontOfSize(24)
            
            textView.font = UIFont.systemFontOfSize(20)
            sendBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
        }
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == phoneTxtField
            {
                self.view.frame.origin.y -= 50
                
            }
            else if textField == messageTxtField
            {
                self.view.frame.origin.y -= 100
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 568
        {
            if textField == messageTxtField
            {
                self.view.frame.origin.y -= 60
                
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 667
        {
            if textField == messageTxtField
            {
                self.view.frame.origin.y -= 40
                
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == messageTxtField
            {
                self.view.frame.origin.y -= 20
                
            }
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == phoneTxtField
            {
                self.view.frame.origin.y += 60
                
            }
            else if textField == messageTxtField
            {
                self.view.frame.origin.y += 100
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        else if sizeHeight == 568
        {
            if textField == messageTxtField
            {
                self.view.frame.origin.y += 60
                
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
            
        else if sizeHeight == 667
        {
            if textField == messageTxtField
            {
                self.view.frame.origin.y += 40
                
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == messageTxtField
            {
                self.view.frame.origin.y += 20
                
            }
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    func isValidEmail(testStr:String) -> Bool {
        
        println("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        var result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    
    
    @IBAction func sendBtn(sender: AnyObject)
    {
        
        if nameTxtField.text == "" || emailTxtField.text == "" || phoneTxtField.text == "" || messageTxtField.text == ""
        {
            let alert = UIAlertView(title: "Alert", message: "Required All fields missing", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        else if !isValidEmail(emailTxtField.text)
        {
            let alert = UIAlertView(title: "Alert", message: "Invalid Email format", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
            
        else
        {
            
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            var post = NSString(format: "carID=%@&txtName=%@&txtEmail=%@&phone=%@&txtMessage=%@", carIdStr,nameTxtField.text,emailTxtField.text,phoneTxtField.text,messageTxtField.text)
            
            
            data = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(data.length)
            
            var url = NSURL(string: "http://www.motortrader.ng/api/enquiryform.php")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = data
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            var connection = NSURLConnection(request: urlRequest, delegate: self)
            
            
        }
        
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.mutableData = NSMutableData(data: data)
    }
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        var error:NSError?
        
        var enquiryData = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        
        
        loaderView.hidden = true
        println(enquiryData)
        
        
        let str = enquiryData.valueForKey("message") as! String
        
        let alert = UIAlertView(title: "Alert", message: str, delegate: self, cancelButtonTitle: "OK")
        alert.show()
        
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        nameTxtField.resignFirstResponder()
        emailTxtField.resignFirstResponder()
        phoneTxtField.resignFirstResponder()
        messageTxtField.resignFirstResponder()
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
