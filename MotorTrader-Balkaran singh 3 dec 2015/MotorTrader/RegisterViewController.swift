//
//  RegisterViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController,UITextFieldDelegate,NSURLConnectionDelegate,UIAlertViewDelegate {
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var loaderView: UIView!
    @IBOutlet var loaderSubView: UIView!
    var data = NSData()
    var alertSuccess = UIAlertView()
    var mutableData = NSMutableData()
    @IBOutlet var repwdTxtField: UITextField!
    @IBOutlet var pwdTxtfield: UITextField!
    @IBOutlet var locationTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    
    @IBOutlet var lastnameTxtField: UITextField!
    @IBOutlet var firstnameTxtField: UITextField!
    @IBOutlet var registerBtn: UIButton!
    @IBOutlet var repwdLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var pwdLbl: UILabel!
    @IBOutlet var emailLbl: UILabel!
    
    @IBOutlet var lastNameLbl: UILabel!
    @IBOutlet var firstNameLbl: UILabel!
    @IBOutlet var fillFormLbl: UILabel!
    
    var loadingMessage = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255, green: 144.0/255, blue: 15.0/255, alpha: 1.0)
        
        firstnameTxtField.delegate = self
        lastnameTxtField.delegate = self
        emailTxtField.delegate = self
        locationTxtField.delegate = self
        pwdTxtfield.delegate = self
        repwdTxtField.delegate = self
        compatibility()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            fillFormLbl.font = UIFont.systemFontOfSize(22)
            firstNameLbl.font = UIFont.systemFontOfSize(18)
            lastNameLbl.font = UIFont.systemFontOfSize(18)
            
            emailLbl.font = UIFont.systemFontOfSize(18)
            pwdLbl.font = UIFont.systemFontOfSize(18)
            locationLbl.font = UIFont.systemFontOfSize(18)
            repwdLbl.font = UIFont.systemFontOfSize(18)
            registerBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            
            
        }
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == locationTxtField
            {
                self.view.frame.origin.y -= 50
                
            }
            else if textField == pwdTxtfield
            {
                self.view.frame.origin.y -= 100
            }
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y -= 150
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 568
        {
            if textField == pwdTxtfield
            {
                self.view.frame.origin.y -= 60
                
            }
                
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y -= 100
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 667
        {
            if textField == pwdTxtfield
            {
                self.view.frame.origin.y -= 40
                
            }
                
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y -= 80
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == pwdTxtfield
            {
                self.view.frame.origin.y -= 20
                
            }
                
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y -= 70
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == locationTxtField
            {
                self.view.frame.origin.y += 60
                
            }
            else if textField == pwdTxtfield
            {
                self.view.frame.origin.y += 100
            }
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y += 150
            }
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        else if sizeHeight == 568
        {
            if textField == pwdTxtfield
            {
                self.view.frame.origin.y += 60
                
            }
                
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y += 100
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
            
        else if sizeHeight == 667
        {
            if textField == pwdTxtfield
            {
                self.view.frame.origin.y += 40
                
            }
                
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y += 80
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == pwdTxtfield
            {
                self.view.frame.origin.y += 20
                
            }
                
            else if textField == repwdTxtField
            {
                self.view.frame.origin.y += 70
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func registerBtn(sender: AnyObject)
    {
        
        if repwdTxtField.text == "" || firstnameTxtField.text == "" || lastnameTxtField.text == "" || emailTxtField.text == "" ||  pwdTxtfield.text == "" || locationTxtField == ""
        {
            let alert = UIAlertView(title: "Alert", message: "Required fields missing", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
            
        else if count(pwdTxtfield.text!) <= 7
        {
            let alert = UIAlertView(title: "Alert", message: "password length too short", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
            
            
        else if repwdTxtField.text != pwdTxtfield.text
        {
            
            let alert = UIAlertView(title: "Alert", message: "Incorrect Re-password", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
        else
        {
            
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            var post = NSString(format:"firstname=%@&lastname=%@&userEmail=%@&userPassword=%@&userlocation=%@" , firstnameTxtField.text,lastnameTxtField.text,emailTxtField.text,pwdTxtfield.text,locationTxtField.text)
            
            
            data = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(data.length)
            
            
            
            
            
            var url = NSURL(string: "http://www.motortrader.ng/api/register.php")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = data
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            var connection = NSURLConnection(request: urlRequest, delegate: self)
            
        }
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.mutableData =  NSMutableData(data: data)
        
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        var error : NSError?
        
        var registorData = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        
        loaderView.hidden = true
        
        let value = registorData.valueForKey("success") as! Bool
        
        if value
        {
            let str = registorData.valueForKey("message") as! String
            alertSuccess = UIAlertView(title: "Alert", message: str, delegate: self, cancelButtonTitle: "Ok")
            alertSuccess.show()
            
            
        }
        else
        {
            
            let str = registorData.valueForKey("message") as! String
            let alert = UIAlertView(title: "Alert", message: str, delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
        
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertSuccess
        {
            if buttonIndex == 0
            {
                self.navigationController?.popViewControllerAnimated(false)
                
            }
            
        }
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        firstnameTxtField.resignFirstResponder()
        lastnameTxtField.resignFirstResponder()
        
        emailTxtField.resignFirstResponder()
        locationTxtField.resignFirstResponder()
        pwdTxtfield.resignFirstResponder()
        repwdTxtField.resignFirstResponder()
    }
    
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)  
    }
    
    
}

