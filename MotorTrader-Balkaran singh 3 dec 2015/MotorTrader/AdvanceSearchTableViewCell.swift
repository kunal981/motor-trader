//
//  AdvanceSearchTableViewCell.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/18/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AdvanceSearchTableViewCell: UITableViewCell {
    
    @IBOutlet var searchPriceLbl: UILabel!
    @IBOutlet var searchLocationLbl: UILabel!
    
    @IBOutlet var searchFuelLbl: UILabel!
    @IBOutlet var seachYearLbl: UILabel!
    @IBOutlet var searchSellerLbl: UILabel!
    
    @IBOutlet var searchMakeLbl: UILabel!
    @IBOutlet var searchImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            searchSellerLbl.font = UIFont.systemFontOfSize(16)
            searchFuelLbl.font = UIFont.systemFontOfSize(16)
            searchLocationLbl.font = UIFont.systemFontOfSize(16)
            seachYearLbl.font = UIFont.systemFontOfSize(16)
            searchImageView.contentMode = UIViewContentMode.ScaleToFill
            
        }
        
    }
    
}
