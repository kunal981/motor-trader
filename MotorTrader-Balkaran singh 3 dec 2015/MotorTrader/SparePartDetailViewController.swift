//
//  SparePartDetailViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/11/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
var strSpareId = NSString()
class SparePartDetailViewController: UIViewController {
    var data = NSData()
    var mutableData = NSMutableData()
    
    @IBOutlet var imageScroll: UIScrollView!
    
    @IBOutlet var loaderSubView: UIView!
    
    @IBOutlet var writeToUsBtn: UIButton!
    @IBOutlet var sellerLbl: UILabel!
    
    @IBOutlet var loaderView: UIView!
    var loadingMessage = UILabel()
    @IBOutlet var modelValueLbl: UILabel!
    @IBOutlet var makeValueLbl: UILabel!
    @IBOutlet var locationValueLbl: UILabel!
    @IBOutlet var yearValueLbl: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var modelLbl: UILabel!
    @IBOutlet var makeLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var yearLbl: UILabel!
    @IBOutlet var sparePriceLbl: UILabel!
    @IBOutlet var spareScrollView: UIScrollView!
    
    @IBOutlet var phoneNolbl: UILabel!
    
    @IBOutlet var spareImageView: UIImageView!
    
    @IBOutlet var spareNameLbl: UILabel!
    
    var imageArray = NSMutableArray()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Spare Part Detail"
        
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        compatibility()
        spareDetailApi()
    }
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func spareDetailApi()
    {
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        loadingMessage.text = "Loading"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderView.hidden = false
        
        
        
        var post = NSString(format: "spareid=%@", strSpareId)
        
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        var url = NSURL(string: "https://www.motortrader.ng/api/parts_detail.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var connection = NSURLConnection(request: urlRequest, delegate: self)
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.mutableData.appendData(data)
    }
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        var error:NSError?
        
        var carDetailData = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        
        println(carDetailData)
        
        if error != nil
        {
            println("\(error?.localizedDescription)")
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),{
                
                self.loaderView.hidden = true
                
                var dict = carDetailData.valueForKey("data") as! NSDictionary
                
                self.spareNameLbl.text = ""
                
                if var spareName = dict.valueForKey("seller_num") as? String
                {
                    self.spareNameLbl.text = spareName
                    
                }
                
                self.sparePriceLbl.text = ""
                
                if var sparePrice = dict.valueForKey("price") as? String
                {
                    
                    self.sparePriceLbl.text = sparePrice
                    
                }
                
                
                self.makeValueLbl.text = ""
                
                if var makeValue = dict.valueForKey("make") as? String
                {
                    
                    
                    self.makeValueLbl.text = makeValue
                }
                
                self.modelValueLbl.text = ""
                if var modelValue = dict.valueForKey("model") as? String
                {
                    
                    
                    self.modelValueLbl.text = modelValue
                }
                
                self.yearValueLbl.text = ""
                
                if var yearvalue = dict.valueForKey("year") as? String
                {
                    
                    self.yearValueLbl.text = yearvalue
                    
                }
                
                
                
                
                
                
                
                
                
                
                var str1 = ""
                var str2 = ""
                
                if var location = dict.valueForKey("location") as? String
                {
                    str1 = location
                    
                }
                
                if var state = dict.valueForKey("state") as? String
                {
                    str2 = state
                }
                
                var str3 =  NSString(format: "%@ (%@)", str1,str2) as? String
                println(str3)
                
                self.locationValueLbl.text = (str3)
                
                self.textView.text = ""
                
                if var txt = dict.valueForKey("desc") as? String
                {
                    
                    
                    self.textView.text = txt
                    
                    let contentSize = self.textView.sizeThatFits(self.textView.bounds.size)
                    var frame = self.textView.frame
                    frame.size.height = contentSize.height
                    self.textView.frame = frame
                    
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
                    {
                        self.textView.font = UIFont.systemFontOfSize(16)
                        
                        
                    }
                    
                    
                    self.spareScrollView.contentSize.height = self.spareScrollView.frame.height + self.textView.frame.height
                }
                
                
                self.phoneNolbl.text = ""
                if var phone  = dict.valueForKey("phone") as? String
                {
                    
                    self.phoneNolbl.text = phone
                    
                }
                
                if  var url =  dict.valueForKey("image") as? String,imagUrl = NSURL(string: url.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                        self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }
                
                
                if  var url2 =  dict.valueForKey("image2") as? String,imagUrl = NSURL(string: url2.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                         self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }

                
                if  var url3 =  dict.valueForKey("image3") as? String,imagUrl = NSURL(string: url3.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                         self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }

                
                
                if  var url4 =  dict.valueForKey("image4") as? String,imagUrl = NSURL(string: url4.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))
                    
                {
                    
                    if var data = NSData(contentsOfURL: imagUrl)
                    {
                        
                         self.imageArray.addObject(UIImage(data: data)!)
                    }
                    
                    
                }
                
                println(self.imageArray)
                
                
                
                self.imageScroll.contentSize.width = CGFloat(self.imageArray.count) * self.imageScroll.frame.width
                
                
                for(var i = 0; i < self.imageArray.count; i++)
                {
                   var image = UIImageView()
                    
                    image.frame = CGRectMake(CGFloat(i) * self.imageScroll.frame.width, 0, self.imageScroll.frame.width, self.imageScroll.frame.height)
                    
                   
                    image.image = self.imageArray[i] as? UIImage
                    
                     image.contentMode = UIViewContentMode.ScaleAspectFit
                    
                    
                    var imageBtn = UIButton()
                    imageBtn = UIButton.buttonWithType(UIButtonType.System) as! UIButton
                    imageBtn.setTitle("", forState: UIControlState.Normal)
                    imageBtn.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
                    
                    
                    imageBtn.frame = image.frame
                    
                    imageBtn.tag = i
                    
                    
                    self.imageScroll.addSubview(image)
                    
                    self.imageScroll.addSubview(imageBtn)
                    
                }
                
                
                

                
            });
            
            
            
            
        }
        
        
        
        
    }
    
    
    func buttonAction(sender:AnyObject)
    {
        let carImages = storyboard?.instantiateViewControllerWithIdentifier("carImages") as! CarImagesViewController
        
        carImages.currentIndex = sender.tag
        
        println(sender.tag)
        
        carImages.imageArray = imageArray
        
        self.navigationController?.pushViewController(carImages, animated: false)
        
    }

    
    
    
    
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            sparePriceLbl.font = UIFont.systemFontOfSize(20)
            yearLbl.font = UIFont.systemFontOfSize(18)
            locationLbl.font = UIFont.systemFontOfSize(18)
            makeLbl.font = UIFont.systemFontOfSize(18)
            modelLbl.font = UIFont.systemFontOfSize(18)
            
            descriptionLbl.font = UIFont.systemFontOfSize(16)
            yearValueLbl.font = UIFont.systemFontOfSize(18)
            locationValueLbl.font = UIFont.systemFontOfSize(18)
            makeValueLbl.font = UIFont.systemFontOfSize(18)
            modelValueLbl.font = UIFont.systemFontOfSize(18)
            textView.font = UIFont.systemFontOfSize(16)
            writeToUsBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            sellerLbl.font = UIFont.systemFontOfSize(14)
            phoneNolbl.font = UIFont.systemFontOfSize(14)
            spareNameLbl.font = UIFont.boldSystemFontOfSize(22)
        }
    }
    
    
    
}
