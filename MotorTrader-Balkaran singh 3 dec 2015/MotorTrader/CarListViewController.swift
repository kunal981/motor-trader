//
//  CarListViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CarListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NSURLConnectionDelegate {
    
    
    @IBOutlet var loaderSubView: UIView!
    @IBOutlet var loaderView: UIView!
    @IBOutlet var addBarButton: UIButton!
    @IBOutlet var barButton: UIButton!
    @IBOutlet var carListLbl: UILabel!
    @IBOutlet var navigationView: UIView!
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var loadingMessage = UILabel()
    
    var carListArray = NSMutableArray()
    var imageCache = [String:UIImage]()
    
    @IBOutlet var carListTableView: UITableView!
    @IBOutlet var menuView: UIView!
    
    override func viewDidLoad() {
        
   
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.navigationBarHidden = true
        
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255, green: 144.0/255, blue: 15.0/255, alpha: 1.0)
        
        menuView.frame = CGRectMake(-355, view.frame.origin.y + 60 , menuView.frame.size.width, menuView.frame.size.height)
        
        carListTableView.frame = CGRectMake(carListTableView.frame.origin.x, navigationView.frame.origin.y + navigationView.frame.size.height, carListTableView.frame.size.width, view.frame.size.height)
        compatibility()
        carListData()
        
        
        if sizeHeight == 480 || sizeHeight == 568 || sizeHeight == 667 || sizeHeight == 736
        {
            navigationView.frame = CGRectMake(navigationView.frame.origin.x, navigationView.frame.origin.y, navigationView.frame.size.width, 60)
        }
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            carListTableView.rowHeight = 200
            
            
        }
        
        
        
        
        
        
        
    }
    
    func compatibility()
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            self.menuView.frame = CGRectMake(-800, view.frame.origin.y + 60, menuView.frame.size.width, menuView.frame.size.height)
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    func carListData()
    {
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        loadingMessage.text = "Loading"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        
        loaderView.hidden = false
        
        
        barButton.userInteractionEnabled = false
        addBarButton.userInteractionEnabled = false
        
        var url = NSURL(string: "http://www.motortrader.ng/api/carlisting.php")
        
        var urlRequest = NSURLRequest(URL: url!)
        
        var connection = NSURLConnection(request: urlRequest, delegate: self)
        
        connection?.start()
        
        
        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue(), completionHandler:{(response : NSURLResponse!,data:NSData!,error:NSError!) -> Void in
            
            
            
            if error == nil
            {
                var err : NSError?
                var objDic = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                self.carListArray = objDic.valueForKey("data") as! NSMutableArray
                
                
                if err != nil
                {
                    println("\(error?.localizedDescription)")
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(),{
                        println(self.carListArray.count)
                        
                        self.loaderView.hidden = true
                        self.barButton.userInteractionEnabled = true
                        self.addBarButton.userInteractionEnabled = true
                        self.carListTableView.reloadData()
                        
                    });
                    
                    
                    
                    
                }
                
                
                
            }
            
            
            
            
            
            
            
        })
        
        
    }
    
    
    
    
    @IBAction func swipe(sender: UISwipeGestureRecognizer)
        
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }
            
        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
        
        
    }
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }

        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        println(carListArray.count)
        return carListArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var customCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCellCarListTableViewCell
        
        
        
        
        var dictionaryCarDetail = carListArray[indexPath.row] as! NSDictionary
        
        println("data...............\(dictionaryCarDetail)")
        
        var strMake = ""
        var strModel = ""
        
        
        if var make = carListArray[indexPath.row].valueForKey("make") as? String
        {
            strMake = make
        }
        
        if var model = carListArray[indexPath.row].valueForKey("model") as? String
        {
            strModel = model
        }
        
        var str3 =  NSString(format: "%@ %@", strMake,strModel) as? String
        println(str3)
        customCell.carNames.text = (str3)
        
        customCell.carPrice.text = ""
        
        
        if var carPrice = carListArray[indexPath.row].valueForKey("price") as? String
        {
            customCell.carPrice.text = carPrice
            
        }
        
        customCell.carModelYear.text = ""
        
        if var carYear = carListArray[indexPath.row].valueForKey("year") as? String
        {
            
            
            customCell.carModelYear.text = carYear
        }
        
        customCell.carLocation.text = ""
        
        if var carLoc = carListArray[indexPath.row].valueForKey("city") as? String
        {
            
            customCell.carLocation.text = carLoc
        }
        
        customCell.carModelNo.text = ""
        
        if var carModelNo = carListArray[indexPath.row].valueForKey("seller_num") as? String
        {
            
            
            customCell.carModelNo.text = carModelNo
        }
        
        customCell.carType.text = ""
        
        if var carType = carListArray[indexPath.row].valueForKey("fuel_type") as? String
        {
            
            
            customCell.carType.text = carType
        }
        
        
        
        
        
        if  var urlString  =  carListArray[indexPath.row].valueForKey("image") as? String, imagUrl = NSURL(string: urlString)
            
        {
            
            
            customCell.carsImage.image = UIImage(named: "")
            
            
            if var img = imageCache[urlString]
                
                
            {
                
                customCell.carsImage.contentMode = UIViewContentMode.ScaleAspectFit
                customCell.carsImage.image = img
                
                
                
                
                
            }
                
            else
            {
                
                let request : NSURLRequest = NSURLRequest(URL: imagUrl)
                
                
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response,data,error) -> Void in
                    
                    
                    if error == nil
                    {
                        
                        let image = UIImage(data: data)
                        
                        self.imageCache[urlString] = image
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            
                            
                            if   let cellUpdate = tableView.cellForRowAtIndexPath(indexPath) as? CustomCellCarListTableViewCell
                            {
                                cellUpdate.carsImage.image = image
                            }
                            
                            
                            
                        })
                        
                    }
                        
                    else
                    {
                        
                        println("Error: \(error.localizedDescription)")
                        
                    }
                    
                })
                
                
            }
            
        }
        
        
        
        
        
        return customCell
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
        }
        else
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
        }
        
        carIdStr = carListArray[indexPath.row].valueForKey("id") as! String
        
        
        println(carIdStr)
        let carDetail = storyboard?.instantiateViewControllerWithIdentifier("carDetail") as? CarDetailViewController
        
        self.navigationController?.pushViewController(carDetail!, animated: true)
    }
    @IBAction func signOut(sender: AnyObject)
    {
        
        var login:LoginViewController = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: false)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "login")
        
        
    }
    
    @IBAction func home(sender: AnyObject)
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
    }
    
    
    @IBAction func searchCar(sender: AnyObject)
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
            
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
            
            
        }
    }
    
    
    @IBAction func searchSpareParts(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    @IBAction func addCar(sender: AnyObject)
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func setting(sender: AnyObject)
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func addSparePart(sender: AnyObject)
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    
    
}
