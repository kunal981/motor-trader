//
//  AddSparePicturesViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/14/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


class AddSparePicturesViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate {
    
    var alertView1 = UIAlertView()
    var alertView2 = UIAlertView()
    var alertView3 = UIAlertView()
    var imagePicker1 = UIImagePickerController()
    var imagePicker2 = UIImagePickerController()
    var imagePicker3 = UIImagePickerController()
    var value1 = Bool()
    var value2 = Bool()
    var value3 = Bool()
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var addPictureLbl: UILabel!
    @IBOutlet var spareImage3: UIImageView!
    @IBOutlet var spareImage2: UIImageView!
    @IBOutlet var spareImage1: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add Spare Part"
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        value1 = false
        value2 = false
        value3 = false
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        
        imagePicker1.delegate = self
        imagePicker2.delegate = self
        imagePicker3.delegate = self
        compatibility()
        
    }
    
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func nextBtn(sender: AnyObject)
    {
        if value1 == true && value2 == true && value3 == true
        {
            let spareSeller = storyboard?.instantiateViewControllerWithIdentifier("spareSeller") as! SpareSellerInfoViewController
            spareAddPicture1 = spareImage1.image!
            spareAddPicture2 = spareImage2.image!
            spareAddPicture3 = spareImage3.image!
            
            self.navigationController?.pushViewController(spareSeller, animated: true)
            
            
            
        }
        else
        {
            
            let alert = UIAlertView(title: "Alert", message: "Please select image", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
            
        }
        
    }
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            addPictureLbl.font = UIFont.systemFontOfSize(18)
            nextBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
            
        }
        
        
        
    }
    
    
    @IBAction func imageBtn1(sender: AnyObject)
    {
        
        
        alertView1 = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertView1.show()
        
        
        
    }
    
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertView1
        {
            
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(UIImagePickerControllerCameraDevice.Rear) != nil)
                {
                    imagePicker1.allowsEditing = true
                    imagePicker1.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePicker1.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePicker1, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePicker1.allowsEditing = true
                imagePicker1.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePicker1, animated: true, completion: nil)
                
            }
        }
            
        else if alertView == alertView2
        {
            
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil)
                {
                    imagePicker2.allowsEditing = true
                    imagePicker2.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePicker2.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePicker2, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePicker2.allowsEditing = true
                imagePicker2.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePicker2, animated: true, completion: nil)
                
            }
            
            
            
            
        }
            
        else if alertView == alertView3
        {
            
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil)
                {
                    imagePicker3.allowsEditing = true
                    imagePicker3.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePicker3.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePicker3, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePicker3.allowsEditing = true
                imagePicker3.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePicker3, animated: true, completion: nil)
                
            }
            
            
            
        }
    }
    
    
    
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        if picker == imagePicker1
        {
            
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                
                spareImage1.contentMode = .ScaleAspectFit
                spareImage1.image = pickedImage
                value1 = true
            }
        }
            
        else if picker == imagePicker2
        {
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                
                spareImage2.contentMode = .ScaleAspectFit
                spareImage2.image = pickedImage
                value2 = true
            }
            
        }
        else
        {
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                
                spareImage3.contentMode = .ScaleAspectFit
                spareImage3.image = pickedImage
                value3 = true
            }
            
            
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func imageBtn2(sender: AnyObject)
    {
        alertView2 = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertView2.show()
    }
    
    
    
    @IBAction func imageBtn3(sender: AnyObject)
    {
        alertView3 = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertView3.show()    }
    
    
}
