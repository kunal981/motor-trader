
//
//  AddCarPictureViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/11/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AddCarPictureViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate {
    var imagePicker1 = UIImagePickerController()
    var imagePicker2 = UIImagePickerController()
    var imagePicker3 = UIImagePickerController()
    var alertView1 = UIAlertView()
    var alertView2 = UIAlertView()
    var alertView3 = UIAlertView()
    @IBOutlet var addPicture3: UIImageView!
    @IBOutlet var addPicture2: UIImageView!
    @IBOutlet var addPicture1: UIImageView!
    @IBOutlet var carPictureLbl: UILabel!
    @IBOutlet var sellerLbl: UILabel!
    @IBOutlet var carDetailLbl: UILabel!
    @IBOutlet var nextBtn: UIButton!
    
    @IBOutlet var addPicLbl: UILabel!
    
    var value1 = Bool()
    var value2 = Bool()
    var value3 = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        value1 = false
        value2 = false
        value3 = false
        self.navigationItem.title = "Add Car"
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        
        
        
        imagePicker1.delegate = self
        imagePicker2.delegate = self
        imagePicker3.delegate = self
        compatibility()
    }
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func compatibility ()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            carDetailLbl.font = UIFont.systemFontOfSize(14)
            carPictureLbl.font = UIFont.systemFontOfSize(14)
            sellerLbl.font = UIFont.systemFontOfSize(14)
            
            addPicLbl.font = UIFont.systemFontOfSize(18)
            
            nextBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
        }
    }
    
    @IBAction func nextBtn(sender: AnyObject)
    {
        
        
        if value1 ==  true && value2 ==  true && value3 ==  true
        {
            let addCarSellerInfo = storyboard?.instantiateViewControllerWithIdentifier("addCarSellerInfo") as! SellerInfoViewController
            
            
            AddPicture1 = addPicture1.image!
            AddPicture2 = addPicture2.image!
            AddPicture3 = addPicture3.image!
            
            
            self.navigationController?.pushViewController(addCarSellerInfo, animated: true)
            
            
            
            
        }
        else
        {
            
            let alert = UIAlertView(title: "Alert", message: "Please select image", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
        }
        
        
        
        
        
        
        
        
        
        
    }
    @IBAction func imageBtn1(sender: AnyObject)
    {
        
        alertView1 = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertView1.show()
    }
    
    
    
    @IBAction func imageBtn2(sender: AnyObject)
    {
        alertView2 = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertView2.show()
    }
    
    
    
    @IBAction func imageBtn3(sender: AnyObject)
    {
        alertView3 = UIAlertView(title:"Alert", message: "Choose the source", delegate: self, cancelButtonTitle: "Camera", otherButtonTitles: "Photo Gallery")
        alertView3.show()
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertView1
        {
            
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(UIImagePickerControllerCameraDevice.Rear) != nil)
                {
                    imagePicker1.allowsEditing = true
                    imagePicker1.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePicker1.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePicker1, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePicker1.allowsEditing = true
                imagePicker1.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePicker1, animated: true, completion: nil)
                
            }
        }
            
        else if alertView == alertView2
        {
            
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil)
                {
                    imagePicker2.allowsEditing = true
                    imagePicker2.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePicker2.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePicker2, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePicker2.allowsEditing = true
                imagePicker2.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePicker2, animated: true, completion: nil)
                
            }
            
            
            
            
        }
            
        else if alertView == alertView3
        {
            
            if buttonIndex == 0
            {
                if (UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil)
                {
                    imagePicker3.allowsEditing = true
                    imagePicker3.sourceType = UIImagePickerControllerSourceType.Camera
                    imagePicker3.cameraCaptureMode = .Photo
                    
                    
                    presentViewController(imagePicker3, animated: true, completion: nil)
                }
                else
                {
                    let alertView = UIAlertView(title:"Alert", message: "Sorry, this device has no camera", delegate: self, cancelButtonTitle: "OK")
                    alertView.show()
                    
                }
                
            }
            else
            {
                
                imagePicker3.allowsEditing = true
                imagePicker3.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                
                presentViewController(imagePicker3, animated: true, completion: nil)
                
            }
            
            
            
        }
    }
    
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        if picker == imagePicker1
        {
            
            
        }
            
        else if picker == imagePicker2
        {
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                
                addPicture2.contentMode = .ScaleAspectFit
                addPicture2.image = pickedImage
                value2 = true
            }
            
        }
        else
        {
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                
                addPicture3.contentMode = .ScaleAspectFit
                addPicture3.image = pickedImage
                value3 = true
            }
            
            
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
