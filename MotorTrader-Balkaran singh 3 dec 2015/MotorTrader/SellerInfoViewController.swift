//
//  SellerInfoViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/11/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Alamofire

var strMakeBtn = NSString()
var strModelBtn = NSString()
var strYearBtn = NSString()
var strTransmissionBtn = NSString()
var strFuelBtn = NSString()
var strBodyBtn = NSString()
var strPriceTxt = NSString()
var strDescriptionTxt = NSString()
var strAddCarState = NSString()
var AddPicture1 = UIImage()
var AddPicture2 = UIImage()
var AddPicture3 = UIImage()




class SellerInfoViewController: UIViewController,UITextFieldDelegate {
    var mutableData = NSMutableData()
    var data = NSData()
    
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var loaderView: UIView!
    
    var loadingMessage = UILabel()
    @IBOutlet var loaderSubView: UIView!
    @IBOutlet var addressTxtField: UITextField!
    @IBOutlet var phoneNoTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var nameTxtField: UITextField!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var phoneLbl: UILabel!
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var fullnameLbl: UILabel!
    @IBOutlet var carPictureLbl: UILabel!
    @IBOutlet var sellerLbl: UILabel!
    @IBOutlet var carDetailLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add Car"
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        
        
        
        
        nameTxtField.delegate =  self
        emailTxtField.delegate = self
        phoneNoTxtField.delegate = self
        addressTxtField.delegate = self
        println(strMakeBtn)
        println(strModelBtn)
        println(strYearBtn)
        println(strTransmissionBtn)
        println(strFuelBtn)
        println(strBodyBtn)
        println(strPriceTxt)
        println(strDescriptionTxt)
        
        println(AddPicture1)
        println(AddPicture2)
        println(AddPicture2)
        
        compatibility()
        
        
        
        
    }
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == emailTxtField
            {
                self.view.frame.origin.y -= 30
                
            }
            else if textField == phoneNoTxtField
            {
                self.view.frame.origin.y -= 80
            }
                
            else if textField == addressTxtField
            {
                self.view.frame.origin.y -= 120
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 568
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y -= 50
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y -= 90
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 667
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y -= 20
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y -= 50
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == addressTxtField
            {
                self.view.frame.origin.y -= 60
                
            }
                
                
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == emailTxtField
            {
                self.view.frame.origin.y += 30
                
            }
            else if textField == phoneNoTxtField
            {
                self.view.frame.origin.y += 80
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y += 120
            }
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        else if sizeHeight == 568
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y += 50
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y += 90
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
            
        else if sizeHeight == 667
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y += 20
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y += 50
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == addressTxtField
            {
                self.view.frame.origin.y += 60
                
            }
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    
    
    
    func createMultipart(image: [UIImage], callback: Bool -> Void){
        
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let strUserId = userDefaults.valueForKey("userID") as? String
        
        let param = [
            "make" : strMakeBtn,
            "model" : strModelBtn,
            "price" : strPriceTxt,
            "transmession" : strTransmissionBtn,
            "desc" : strDescriptionTxt,
            "userId" : strUserId!,
            "sellername" : nameTxtField.text,
            "email" : emailTxtField.text,
            
            "phone" : phoneNoTxtField.text,
            "address" : addressTxtField.text,
            "year" : strYearBtn,
            "fuel" : strFuelBtn,
            "body" : strBodyBtn,
            "state" : strAddCarState
        ]
        
        print(param)
        
        upload(
            .POST,
            URLString: "http://www.motortrader.ng/api/car_post.php",
            multipartFormData: { multipartFormData in
                
                for ( var i = 0; i < image.count; i++)
                {
                    let imageData = UIImagePNGRepresentation(image[i])
                    multipartFormData.appendBodyPart(data: imageData, name: "image\(i+1)", fileName: "iosFile\(i+1).png", mimeType: "image/*")
                }
                
                
                for (key, value) in param {
                    multipartFormData.appendBodyPart(data:"\(value)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :key)
                }
                
                
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { request, response, data, error in
                        
                        println("json:: \(data)")
                        
                        
                        let alert = UIAlertView(title: "Alert", message: "Car Added Successfully", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        self.loaderView.hidden = true
                        
                        
                        callback(true)
                    }
                case .Failure(let encodingError):
                    callback(false)
                }
            }
        )
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        nameTxtField.resignFirstResponder()
        emailTxtField.resignFirstResponder()
        phoneNoTxtField.resignFirstResponder()
        addressTxtField.resignFirstResponder()
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        println("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        var result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    @IBAction func submit(sender: AnyObject)
    {
        
        if nameTxtField.text == "" || emailTxtField.text == "" || phoneNoTxtField.text == "" || addressTxtField.text == ""
            
        {
            let alert = UIAlertView(title: "Alert", message: "Fill all fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
        }
        else if !isValidEmail(emailTxtField.text)
        {
            let alert = UIAlertView(title: "Alert", message: "Invalid Email format", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
            
        else
        {
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            
            createMultipart([AddPicture1, AddPicture2, AddPicture3], callback: { success in
                if success
                {
                    println("success")
                }
                else
                {
                    println("fail")
                }
            })
            
            
        }
    }
    
    func compatibility ()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            fullnameLbl.font = UIFont.systemFontOfSize(18)
            emailLbl.font = UIFont.systemFontOfSize(18)
            phoneLbl.font = UIFont.systemFontOfSize(18)
            addressLbl.font = UIFont.systemFontOfSize(18)
            carDetailLbl.font = UIFont.systemFontOfSize(14)
            carPictureLbl.font = UIFont.systemFontOfSize(14)
            sellerLbl.font = UIFont.systemFontOfSize(14)
            
            
            
            submitBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
        }
    }
    
}


