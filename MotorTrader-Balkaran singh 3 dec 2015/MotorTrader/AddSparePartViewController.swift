//
//  AddSparePartViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/14/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class AddSparePartViewController: UIViewController,UITextFieldDelegate {
    
    
    
    
    
    
    var strLocation = NSString()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var strYear = NSString()
    var loadingMessage = UILabel()
    
    var strMake = NSString()
    
    
    
    var strModel = NSString()
    
    
    
    
    var makedata = NSMutableData()
    var modelData = NSData()
    var makeListingArray = NSMutableArray()
    var modelListingArray = NSMutableArray()
    var locationListingArray = NSMutableArray()
    @IBOutlet var spareSateLbl: UILabel!
    
    @IBOutlet var spareStateBtn: UIButton!
    
    @IBOutlet var spareStateImage: UIImageView!
    
    @IBOutlet var spareStateTableView: UITableView!
    
    @IBOutlet var spareYearBtn: UIButton!
    @IBOutlet var spareYearLbl: UILabel!
    @IBOutlet var spareModelImage: UIImageView!
    @IBOutlet var sparePartImage: UIImageView!
    
    @IBOutlet var spareYearImage: UIImageView!
    
    @IBOutlet var spareMakeImage: UIImageView!
    
    @IBOutlet var spareNextBtn: UIButton!
    @IBOutlet var spareModelLbl: UILabel!
    
    @IBOutlet var spareDescriptionLbl: UILabel!
    @IBOutlet var sparePriceLbl: UILabel!
    
    @IBOutlet var sparePartLbl: UILabel!
    @IBOutlet var spareMakeLbl: UILabel!
    
    @IBOutlet var spareDescriptionTxtDield: UITextField!
    
    @IBOutlet var sparePriceTxtField: UITextField!
    
    @IBOutlet var spareModelBtn: UIButton!
    
    
    @IBOutlet var spareMakeBtn: UIButton!
    @IBOutlet var sparePartTxtField: UITextField!
    @IBOutlet var spareModelTableView: UITableView!
    
    @IBOutlet var spareMakeTableView: UITableView!
    
    
    @IBOutlet var spareYearTableView: UITableView!
    var yearArray = NSArray()
    
    
    @IBOutlet var loaderSubView: UIView!
    
    @IBOutlet var loaderView: UIView!
    
    
    
    @IBOutlet var menuView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = "Add Spare Part"
        self.navigationController?.navigationBarHidden = false
        sparePriceTxtField.delegate = self
        spareDescriptionTxtDield.delegate = self
        sparePartTxtField.delegate = self
        
        
        
        
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        menuView.frame = CGRectMake(-355, view.frame.origin.y+60 , menuView.frame.size.width, menuView.frame.size.height)
        
        spareStateTableView.frame = CGRectMake(spareStateBtn.frame.origin.x, spareStateBtn.frame.origin.y, spareStateBtn.frame.size.width
            , 150)
        spareStateTableView.layer.borderWidth = 1
        
        spareMakeTableView.frame = CGRectMake(spareMakeBtn.frame.origin.x, spareMakeBtn.frame.origin.y, spareMakeBtn.frame.size.width
            , 150)
        spareMakeTableView.layer.borderWidth = 1
        
        spareModelTableView.frame = CGRectMake(spareModelBtn.frame.origin.x, spareModelBtn.frame.origin.y, spareModelBtn.frame.size.width
            , 150)
        spareModelTableView.layer.borderWidth = 1
        
        spareYearTableView.frame = CGRectMake(spareYearBtn.frame.origin.x, spareYearBtn.frame.origin.y, spareYearBtn.frame.size.width
            , 150)
        spareYearTableView.layer.borderWidth = 1
        yearArray = ["2015","2014","2013","2012","2011","2014","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997",
            "1996", "1995", "1994", "1993",
            "1992"]
        
        compatibility()
        makeApiData()
        locationApiData()
    }
    
    
    func locationApiData()
    {
        
        var urlBody = NSURL(string: "https://www.motortrader.ng/api/locationlist.php")
        
        var urlRequest = NSURLRequest(URL: urlBody!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.spareStateTableView.reloadData()
                    
                    
                    
                    self.locationListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.locationListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
        
        
        
        
        
        
    }
    
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y -= 140
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y -= 180
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 568
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y -= 100
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y -= 150
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 667
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y -= 100
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y -= 140
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y -= 100
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y -= 140
            }
                
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y += 140
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y += 180
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        else if sizeHeight == 568
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y += 100
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y += 150
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
            
        else if sizeHeight == 667
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y += 100
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y += 140
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == sparePriceTxtField
            {
                self.view.frame.origin.y += 100
                
            }
            else if textField == spareDescriptionTxtDield
            {
                self.view.frame.origin.y += 140
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    
    func compatibility()
    {
        
        if  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            self.menuView.frame = CGRectMake(-800, view.frame.origin.y + 60, menuView.frame.size.width, menuView.frame.size.height)
            sparePartLbl.font = UIFont.systemFontOfSize(18)
            spareMakeLbl.font = UIFont.systemFontOfSize(18)
            spareModelLbl.font = UIFont.systemFontOfSize(18)
            spareYearLbl.font = UIFont.systemFontOfSize(18)
            sparePriceLbl.font = UIFont.systemFontOfSize(18)
            spareDescriptionLbl.font = UIFont.systemFontOfSize(18)
            
            
            spareStateImage.frame = CGRectMake(spareStateImage.frame.origin.x, spareStateImage.frame.origin.y-4, spareStateImage.frame.size.width, spareStateImage.frame.size.height)
            
            
            sparePartImage.frame = CGRectMake(sparePartImage.frame.origin.x+3, sparePartImage.frame.origin.y-2, sparePartImage.frame.size.width, sparePartImage.frame.size.height)
            
            spareMakeImage.frame = CGRectMake(spareMakeImage.frame.origin.x+3, spareMakeImage.frame.origin.y-2, spareMakeImage.frame.size.width, spareMakeImage.frame.size.height)
            
            spareModelImage.frame = CGRectMake(spareModelImage.frame.origin.x+3, spareModelImage.frame.origin.y-2, spareModelImage.frame.size.width, spareModelImage.frame.size.height)
            
            spareYearImage.frame = CGRectMake(spareYearImage.frame.origin.x, spareYearImage.frame.origin.y-4, spareYearImage.frame.size.width, spareYearImage.frame.size.height)
            spareNextBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            
            
            
            spareStateTableView.frame = CGRectMake(spareStateBtn.frame.origin.x, spareStateBtn.frame.origin.y, spareStateBtn.frame.size.width
                , 250)
            
            spareMakeTableView.frame = CGRectMake(spareMakeBtn.frame.origin.x, spareMakeBtn.frame.origin.y, spareMakeBtn.frame.size.width
                , 250)
            
            spareModelTableView.frame = CGRectMake(spareModelBtn.frame.origin.x, spareModelBtn.frame.origin.y, spareModelBtn.frame.size.width
                , 250)
            
            spareYearTableView.frame = CGRectMake(spareYearBtn.frame.origin.x, spareYearBtn.frame.origin.y, spareYearBtn.frame.size.width
                , 250)
            
            
            
            
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func spareMakeBtn(sender: AnyObject)
    {
        spareMakeTableView.reloadData()
        spareMakeTableView.hidden = false
        spareModelTableView.hidden = true
        spareYearTableView.hidden = true
        spareStateTableView.hidden = true
    }
    
    @IBAction func spareStateBtn(sender: AnyObject)
    {
        spareStateTableView.reloadData()
        spareMakeTableView.hidden = true
        spareModelTableView.hidden = true
        spareYearTableView.hidden = true
        spareStateTableView.hidden = false
    }
    
    
    
    @IBAction func spareModelBtn(sender: AnyObject)
    {
        spareModelTableView.reloadData()
        spareMakeTableView.hidden = true
        spareModelTableView.hidden = false
        spareYearTableView.hidden = true
        spareStateTableView.hidden = true
        
    }
    
    
    
    @IBAction func spareYearBtn(sender: AnyObject)
        
    {
        spareMakeTableView.hidden = true
        spareModelTableView.hidden = true
        spareYearTableView.hidden = false
        spareStateTableView.hidden = true
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        spareMakeTableView.hidden = true
        spareModelTableView.hidden = true
        spareYearTableView.hidden = true
        spareStateTableView.hidden = true
        sparePriceTxtField.resignFirstResponder()
        spareDescriptionTxtDield.resignFirstResponder()
        sparePartTxtField.resignFirstResponder()
    }
    
    
    func modelApiData(makeid:NSString)
    {
        
        println(makeid)
        
        var post = NSString(format: "makeId=%@", makeid)
        
        modelData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(modelData.length)
        
        var url = NSURL(string: "http://www.motortrader.ng/api/findModel.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = modelData
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.spareModelTableView.reloadData()
                    
                    
                    
                    self.modelListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.modelListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
        
        
    }
    func makeApiData()
    {
        
        
        var urlMake = NSURL(string: "http://www.motortrader.ng/api/findmake.php")
        
        var urlRequest = NSURLRequest(URL: urlMake!)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler:{ data,response,error -> Void in
            
            
            
            
            
            var error:NSError?
            
            var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
            println(dicObj)
            
            if error != nil
            {
                println("\(error?.localizedDescription)")
            }
            else
            {
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    
                    self.spareMakeTableView.reloadData()
                    
                    
                    
                    self.makeListingArray = dicObj.valueForKey("data") as! NSMutableArray
                    
                    println(self.makeListingArray)
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
            }
            
        });
        task.resume()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == spareMakeTableView
        {
            return makeListingArray.count
        }
        else if tableView == spareModelTableView
        {
            return modelListingArray.count
        }
        else if tableView == spareYearTableView
        {
            return yearArray.count
        }
        else
        {
            
            return locationListingArray.count
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        
        if tableView == spareMakeTableView
        {
            var makeCell = tableView.dequeueReusableCellWithIdentifier("makeCell", forIndexPath: indexPath) as! UITableViewCell
            
            if var id = makeListingArray[indexPath.row].valueForKey("id") as? String
            {
                strMake = id
            }
            
            makeCell.textLabel?.text = ""
            
            if var make = makeListingArray[indexPath.row].valueForKey("name") as? String
            {
                makeCell.textLabel?.text = make
            }
            makeCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return makeCell
            
            
        }
        else if tableView == spareModelTableView
        {
            var modelCell = tableView.dequeueReusableCellWithIdentifier("modelCell", forIndexPath: indexPath) as! UITableViewCell
            
            if var id = modelListingArray[indexPath.row].valueForKey("id") as? String
            {
                strModel = id
            }
            
            modelCell.textLabel?.text = ""
            
            if var model = modelListingArray[indexPath.row].valueForKey("model") as? String
            {
                
                modelCell.textLabel?.text = model
            }
            modelCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return modelCell
        }
        else if tableView == spareYearTableView
        {
            var yearCell = tableView.dequeueReusableCellWithIdentifier("yearCell", forIndexPath: indexPath) as! UITableViewCell
            
            yearCell.textLabel?.text = yearArray[indexPath.row] as? String
            yearCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return yearCell
            
        }
        else
        {
            var locationCell = tableView.dequeueReusableCellWithIdentifier("locationCell", forIndexPath: indexPath) as! UITableViewCell
            locationCell.textLabel?.text = ""
            
            if var loc = locationListingArray[indexPath.row].valueForKey("name") as? String
            {
                locationCell.textLabel?.text = loc
                
            }
            
            locationCell.textLabel?.font = UIFont.systemFontOfSize(12)
            
            return locationCell
            
            
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == spareMakeTableView
            
        {
            
            
            
            modelApiData((makeListingArray[indexPath.row].valueForKey("id") as? String)!)
            
            
            spareMakeBtn.setTitle(makeListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            spareMakeTableView.hidden = true
            
            
        }
        else if tableView == spareModelTableView
        {
            
            spareModelBtn.setTitle(modelListingArray[indexPath.row].valueForKey("model") as? String, forState: UIControlState.Normal)
            spareModelTableView.hidden = true
            
        }
        else if tableView == spareYearTableView
        {
            
            strYear = NSString(format: "%i",indexPath.row)
            
            println(strYear)
            
            spareYearBtn.setTitle(yearArray[indexPath.row] as? String, forState: UIControlState.Normal)
            spareYearTableView.hidden = true
            
            
        }
        else
        {
            
            
            strLocation = (locationListingArray[indexPath.row].valueForKey("id") as? String)!
            
            println(strLocation)
            
            spareStateBtn.setTitle(locationListingArray[indexPath.row].valueForKey("name") as? String, forState: UIControlState.Normal)
            spareStateTableView.hidden = true
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    @IBAction func barButtonMenu(sender: AnyObject)
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            if self.menuView.frame.origin.x == -800
            {
                
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                })
                
                
            }
                
                
            else
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-800
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
            
            
        }
            
        else
            
        {
            if self.menuView.frame.origin.x == -355
            {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(self.view.frame.origin.x,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                    
                    
                    
                    
                });
            }
                
            else           {
                UIView.animateWithDuration(0.5, animations:{
                    
                    
                    
                    self.menuView.frame = CGRectMake(-355
                        ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
                })
            }
            
            
        }
        
    }
    @IBAction func home(sender: AnyObject)
    {
        
        var carlist = storyboard?.instantiateViewControllerWithIdentifier("carList") as! CarListViewController
        
        self.navigationController?.pushViewController(carlist, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
    }
    
    
    @IBAction func searchCar(sender: AnyObject)
    {
        
        var searchCar = storyboard?.instantiateViewControllerWithIdentifier("searchCar") as! AdvanceSearchViewController
        
        self.navigationController?.pushViewController(searchCar, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
            
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
            
            
            
            
        }
    }
    
    
    @IBAction func searchSpareParts(sender: AnyObject)
    {
        var sparePart = storyboard?.instantiateViewControllerWithIdentifier("sparePart") as! SparePartViewController
        
        self.navigationController?.pushViewController(sparePart, animated: false)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    
    @IBAction func addCar(sender: AnyObject)
    {
        var addCar = storyboard?.instantiateViewControllerWithIdentifier("addCar") as! AddCarDetailViewController
        
        self.navigationController?.pushViewController(addCar, animated: false)
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func setting(sender: AnyObject)
    {
        var setting = storyboard?.instantiateViewControllerWithIdentifier("setting") as! SettingViewController
        
        self.navigationController?.pushViewController(setting, animated: false)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
        else
        {
            
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func addSparePart(sender: AnyObject)
    {
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-800
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
            
            
        else
        {
            
            UIView.animateWithDuration(0.5, animations:{
                
                
                
                self.menuView.frame = CGRectMake(-355
                    ,self.menuView.frame.origin.y, self.menuView.frame.size.width, self.menuView.frame.size.height)
            })
            
        }
        
    }
    
    @IBAction func signOut(sender: AnyObject)
    {
        
        var login:LoginViewController = storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: false)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "login")
        
        
    }
    
    
    @IBAction func nextBtn(sender: AnyObject)
    {
        
        
        if sparePartTxtField.text == "" || spareMakeBtn.titleLabel?.text == "Any Make" || spareModelBtn.titleLabel?.text == "Any Model" || spareYearBtn.titleLabel?.text == "Any Year" || spareStateBtn.titleLabel?.text == "Any Sate" || sparePriceTxtField.text == "" || spareDescriptionTxtDield.text ==  ""
        {
            let alert = UIAlertView(title: "Alert", message: "Select all fields", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
        else
        {
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            
            strSparePartTxt = sparePartTxtField.text
            strSpareMakeBtn = strMake
            strSpareModelBtn = strModel
            strSpareYearBtn = spareYearBtn.titleLabel!.text!
            strSparePriceTxt = sparePriceTxtField.text
            strSpareDescriptionTxt = spareDescriptionTxtDield.text
            strSpareStateBtn = strLocation
            println(strSparePartTxt)
            println(strSpareMakeBtn)
            println(strSpareModelBtn)
            println(strSpareYearBtn)
            println(strSparePriceTxt)
            println(strSpareDescriptionTxt)
            println(strSpareStateBtn)
            
            let addSparePicture = storyboard?.instantiateViewControllerWithIdentifier("addSparePicture") as! AddSparePicturesViewController
            loaderView.hidden = true
            self.navigationController?.pushViewController(addSparePicture, animated: true)
            
            
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
