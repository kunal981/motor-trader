//
//  CustomCellCarListTableViewCell.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CustomCellCarListTableViewCell: UITableViewCell {
    
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var cellBackView: UIView!
    @IBOutlet var carModelYearImage: UIImageView!
    @IBOutlet var carLocationImage: UIImageView!
    @IBOutlet var carTypeImage: UIImageView!
    @IBOutlet var carModelNoImage: UIImageView!
    @IBOutlet var carModelYear: UILabel!
    @IBOutlet var carLocation: UILabel!
    @IBOutlet var carType: UILabel!
    @IBOutlet var carModelNo: UILabel!
    @IBOutlet var carPrice: UILabel!
    @IBOutlet var carNames: UILabel!
    @IBOutlet var carsImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            carModelNo.font = UIFont.systemFontOfSize(14)
            carType.font = UIFont.systemFontOfSize(14)
            carLocation.font = UIFont.systemFontOfSize(14)
            carModelYear.font = UIFont.systemFontOfSize(14)
            carNames.font = UIFont.boldSystemFontOfSize(16)
            carPrice.font = UIFont.systemFontOfSize(16)
            carsImage.contentMode = UIViewContentMode.ScaleAspectFit
            
        }
        
        
        
        
        
    }
    
}
