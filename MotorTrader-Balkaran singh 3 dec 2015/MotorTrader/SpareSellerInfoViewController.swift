//
//  SpareSellerInfoViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/14/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import Alamofire

var strSpareMakeBtn = NSString()
var strSpareStateBtn = NSString()
var strSpareModelBtn = NSString()
var strSpareYearBtn = NSString()
var strSparePriceTxt = NSString()
var strSparePartTxt = NSString()
var strSpareDescriptionTxt = NSString()
var spareAddPicture1 = UIImage()
var spareAddPicture2 = UIImage()
var spareAddPicture3 = UIImage()







class SpareSellerInfoViewController: UIViewController,UITextFieldDelegate {
    var data = NSData()
    
    
    var sizeHeight = UIScreen.mainScreen().bounds.height
    @IBOutlet var addressTxtField: UITextField!
    @IBOutlet var phoneNoTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var fullNameTxtField: UITextField!
    var loadingMessage = UILabel()
    @IBOutlet var loaderView: UIView!
    @IBOutlet var loaderSubView: UIView!
    
    @IBOutlet var fullNameLbl: UILabel!
    var mutableData = NSMutableData()
    
    @IBOutlet var emailLbl: UILabel!
    
    @IBOutlet var phoneNoLbl: UILabel!
    
    @IBOutlet var addressLbl: UILabel!
    
    
    @IBOutlet var submitLbl: UIButton!
    
    
    override func viewDidLoad() {
        
        self.navigationItem.title = "Add Spare Part"
        let barButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        
        
        
        var barButtonItem : UIBarButtonItem = UIBarButtonItem()
        
        barButtonItem = UIBarButtonItem(image: UIImage(named: "1440409654_back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBackBtn:")
        
        barButtonItem.tintColor = UIColor.whiteColor()
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        
        
        
        
        fullNameTxtField.delegate = self
        emailTxtField.delegate = self
        phoneNoTxtField.delegate = self
        addressTxtField.delegate = self
        super.viewDidLoad()
        println(strSpareMakeBtn)
        println(strSpareModelBtn)
        println(strSpareYearBtn)
        println(strSpareStateBtn)
        println(strSparePriceTxt)
        println(strSparePartTxt)
        println(strSpareDescriptionTxt)
        println(spareAddPicture1)
        println(spareAddPicture2)
        println(spareAddPicture3)
        compatibility()
        
        
    }
    
    func leftBackBtn (sender:UIBarButtonItem)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == emailTxtField
            {
                self.view.frame.origin.y -= 30
                
            }
            else if textField == phoneNoTxtField
            {
                self.view.frame.origin.y -= 80
            }
                
            else if textField == addressTxtField
            {
                self.view.frame.origin.y -= 120
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 568
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y -= 50
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y -= 90
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else if sizeHeight == 667
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y -= 20
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y -= 50
            }
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == addressTxtField
            {
                self.view.frame.origin.y -= 60
                
            }
                
                
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        if sizeHeight == 480
        {
            if textField == emailTxtField
            {
                self.view.frame.origin.y += 30
                
            }
            else if textField == phoneNoTxtField
            {
                self.view.frame.origin.y += 80
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y += 120
            }
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
            
        }
        else if sizeHeight == 568
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y += 50
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y += 90
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
            
        else if sizeHeight == 667
        {
            if textField == phoneNoTxtField
            {
                self.view.frame.origin.y += 20
                
            }
            else if textField == addressTxtField
            {
                self.view.frame.origin.y += 50
            }
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
        }
        else
        {
            if textField == addressTxtField
            {
                self.view.frame.origin.y += 60
                
            }
                
                
                
            else
            {
                self.view.frame.origin.y = self.view.frame.origin.y
            }
            
            
            
        }
        
        
        return true
    }
    
    
    
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        fullNameTxtField.resignFirstResponder()
        emailTxtField.resignFirstResponder()
        phoneNoTxtField.resignFirstResponder()
        addressTxtField.resignFirstResponder()
    }
    func compatibility()
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            fullNameLbl.font = UIFont.systemFontOfSize(18)
            emailLbl.font = UIFont.systemFontOfSize(18)
            phoneNoLbl.font = UIFont.systemFontOfSize(18)
            addressLbl.font = UIFont.systemFontOfSize(18)
            submitLbl.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
        }
        
    }
    
    func createMultipart(image: [UIImage], callback: Bool -> Void){
        
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let strUserId = userDefaults.valueForKey("userID") as? String
        
        let param = [
            "sparename" : strSparePartTxt,
            "make" : strSpareMakeBtn,
            "model" : strSpareModelBtn,
            "price" : strSparePriceTxt,
            "year" : strSpareYearBtn,
            "desc" : strSpareDescriptionTxt,
            "userId" : strUserId!,
            "sellername" : fullNameTxtField.text,
            "email" : emailTxtField.text,
            
            "phone" : phoneNoTxtField.text,
            "address" : addressTxtField.text,
            "state" : strSpareStateBtn
        ]
        
        
        
        upload(
            .POST,
            URLString: "http://www.motortrader.ng/api/spare_post.php",
            multipartFormData: { multipartFormData in
                
                for ( var i = 0; i < image.count; i++)
                {
                    let imageData = UIImagePNGRepresentation(image[i])
                    multipartFormData.appendBodyPart(data: imageData, name: "image\(i+1)", fileName: "iosFile\(i+1).png", mimeType: "image/*")
                }
                
                
                for (key, value) in param {
                    multipartFormData.appendBodyPart(data:"\(value)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :key)
                }
                
                
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { request, response, data, error in
                        //let json = JSON(data!)
                        println("json:: \(data)")
                        self.loaderView.hidden = true
                        let alert = UIAlertView(title: "Alert", message: "Spare Part Added Successfully", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        
                        callback(true)
                    }
                case .Failure(let encodingError):
                    callback(false)
                }
            }
        )
    }
    func isValidEmail(testStr:String) -> Bool {
        
        println("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        var result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    
    
    @IBAction func submitBtn(sender: AnyObject)
    {
        if fullNameTxtField.text == "" || emailTxtField.text == "" || phoneNoTxtField.text == "" || addressTxtField.text == ""
            
        {
            let alert = UIAlertView(title: "Alert", message: "Fill all fields", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
        }
        else if !isValidEmail(emailTxtField.text)
        {
            let alert = UIAlertView(title: "Alert", message: "Invalid Email format", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
            
        else
        {
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            
            createMultipart([spareAddPicture1, spareAddPicture2, spareAddPicture3], callback: { success in
                if success
                {
                    println("success")
                }
                else
                {
                    println("fail")
                }
            })
            
            
        }
        
    }
    
}


