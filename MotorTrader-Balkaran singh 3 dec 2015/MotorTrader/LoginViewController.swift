//
//  LoginViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate,NSURLConnectionDelegate {
    var data = NSData()
    var loginData = NSDictionary()
    var mutableData = NSMutableData()
    @IBOutlet var usernameView: UIView!
    @IBOutlet var pwdLineView: UIView!
    @IBOutlet var registerBtn: UIButton!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var forgotPwdBtn: UIButton!
    @IBOutlet var passwordLbl: UILabel!
    @IBOutlet var usernameLbl: UILabel!
    @IBOutlet var pwdTxtField: UITextField!
    @IBOutlet var userNameTxtField: UITextField!
    var loadingMessage = UILabel()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    
    @IBOutlet var loaderView: UIView!
    
    @IBOutlet var loaderSubView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255, green: 144.0/255, blue: 15.0/255, alpha: 1.0)
        
        // userNameTxtField.becomeFirstResponder()
        userNameTxtField.delegate = self
        pwdTxtField.delegate = self
        
        compatibility()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        userNameTxtField.resignFirstResponder()
        pwdTxtField.resignFirstResponder()
    }
    
    func compatibility()
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            usernameLbl.font = UIFont.systemFontOfSize(22)
            passwordLbl.font = UIFont.systemFontOfSize(22)
            forgotPwdBtn.titleLabel?.font = UIFont.systemFontOfSize(22)
            loginBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            registerBtn.titleLabel?.font = UIFont.systemFontOfSize(22)
            usernameView.frame = CGRectMake(usernameView.frame.origin.x, usernameView.frame.origin.y, usernameView.frame.size.width, 3)
            userNameTxtField.font = UIFont.systemFontOfSize(18)
            pwdTxtField.font = UIFont.systemFontOfSize(18)
        }
        
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if sizeHeight == 480
        {
            if textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y -= 60
            }
            
        }
        else if sizeHeight == 568
        {
            if  textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y -= 50
            }
            
            
        }
        else
        {
            
            self.view.frame.origin.y = self.view.frame.origin.y
            
            
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if sizeHeight == 480
        {
            if textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y += 60
                
            }
            
            
        }
            
        else if sizeHeight == 568
        {
            if  textField == userNameTxtField || textField == pwdTxtField
            {
                self.view.frame.origin.y += 50
            }
            
            
        }
        else
        {
            
            self.view.frame.origin.y = self.view.frame.origin.y
            
            
        }
        
        
        return true
    }
    
    @IBAction func loginBtn(sender: AnyObject)
    {
        
        if userNameTxtField.text == "" || pwdTxtField.text == ""
        {
            
            let alert = UIAlertView(title: "Alert", message: "Required Fill all textfields", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
        else
        {
            
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            loaderSubView.addSubview(indicator)
            
            indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
            
            loadingMessage.text = "Loading"
            loadingMessage.textColor = UIColor.whiteColor()
            
            loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
            loadingMessage.numberOfLines = 2
            loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
            
            loadingMessage.textAlignment = .Center
            
            loaderSubView.addSubview(loadingMessage)
            
            loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
            
            
            
            
            
            
            
            indicator.startAnimating()
            loaderView.hidden = false
            
            
            
            
            var post = NSString(format: "email=%@&password=%@", userNameTxtField.text,pwdTxtField.text)
            
            println(post)
            
            data = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(data.length)
            
            let url = NSURL(string: "http://www.motortrader.ng/api/login.php")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = data
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            var connection = NSURLConnection(request: urlRequest, delegate: self)
            
        }
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        
        self.mutableData = NSMutableData(data: data)
        
        
    }
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        println(mutableData)
        var error:NSError?
        
        loginData = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.AllowFragments, error: &error) as! NSDictionary
        
        println(loginData)
        
        var value = loginData.valueForKey("success") as! Bool
        
        if value
        {
            
            var strUserId = loginData.valueForKey("userId") as! String
            println(strUserId)
            
            var userIdStore = NSUserDefaults.standardUserDefaults()
            userIdStore.setObject(strUserId, forKey: "userID")
            
            let userdef = NSUserDefaults.standardUserDefaults()
            userdef.setBool(true, forKey: "login")
            loaderView.hidden = true
            var carList = storyboard?.instantiateViewControllerWithIdentifier("carList") as! CarListViewController
            
            self.navigationController?.pushViewController(carList, animated: false)
            
            
        }
        else
        {
            
            
            loaderView.hidden = true
            
            let strMessage = loginData.valueForKey("message") as! String
            
            let alert = UIAlertView(title: "Alert", message: strMessage, delegate: self, cancelButtonTitle:"OK")
            alert.show()
            
        }
        
        
        
    }
    
    
    
    
}
