//
//  ForgotPwdViewController.swift
//  MotorTrader
//
//  Created by mrinal khullar on 8/10/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ForgotPwdViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate {
    
    
    @IBOutlet var loaderView: UIView!
    
    @IBOutlet var loaderSubView: UIView!
    
    var data = NSData()
    var alertSuccess = UIAlertView()
    var mutableData = NSMutableData()
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var txtView: UITextView!
    @IBOutlet var forgotPwdLbl: UILabel!
    @IBOutlet var emailTxtField: UITextField!
    
    var loadingMessage = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255, green: 144.0/255, blue: 15.0/255, alpha: 1.0)
        
        emailTxtField.delegate = self
        compatibility()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    func compatibility()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            forgotPwdLbl.font = UIFont.systemFontOfSize(22)
            txtView.font = UIFont.systemFontOfSize(18)
            emailLbl.font = UIFont.systemFontOfSize(20)
            submitBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            emailTxtField.font = UIFont.systemFontOfSize(18)
            
        }
    }
    
    @IBAction func submitBtn(sender: AnyObject)
    {
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        loadingMessage.text = "Loading"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        loaderView.hidden = false
        
        
        
        var post = NSString(format:"userEmail=%@" , emailTxtField.text)
        
        
        data = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(data.length)
        
        
        
        
        
        var url = NSURL(string: "http://www.motortrader.ng/api/forgotpass.php")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = data
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        var connection = NSURLConnection(request: urlRequest, delegate: self)
        
        
    }
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.mutableData = NSMutableData(data: data)
        
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        var error : NSError?
        
        var forgotData = NSJSONSerialization.JSONObjectWithData(mutableData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        
        println(forgotData)
        loaderView.hidden = true
        
        let value = forgotData.valueForKey("success") as! Bool
        
        if value
        {
            alertSuccess = UIAlertView(title: "Alert", message: "Password sent to your email", delegate: self, cancelButtonTitle: "Ok")
            alertSuccess.show()
            
        }
        else
            
        {
            let str = forgotData.valueForKey("message") as! String
            
            let alert = UIAlertView(title: "Alert", message: str, delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
            
            
            
            
            
            
            
        }
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        emailTxtField.resignFirstResponder()
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView == alertSuccess
        {
            if buttonIndex == 0
            {
                self.navigationController?.popViewControllerAnimated(false)
                
            }
            
        }
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
}
